/**
  GENERAL NOTES ON LIBRARIES USED AND SOME CONFIGURATIONS OF THE GENERAL APPLICATION
*/

- At the time of writing this documentation TEST for the app was yet to be written and so does not make mention of it

- Use npm install to pull in all dependencies

- Use nodemon server.js to serve the app

- The redis session store is provided by the heroku Redis To Go add-on

- In the package.json file the under scripts there is a production property. It is used to restart  the server in case
    of any unexpected errors causing the server to crash.

- Formidable library is used to process file uploads

- You will need to set  some of the following variables in the config folder depending on your environment

 "DB_URL_DEVELOPMENT": "",
  "DB_PROD": "", // This was created when the orginal DB was in maintenance and was not accessible
  "DB_URL_PRODUCTION": "",
  "DB_UNOSIETE_LIVE": "",
  "AUTH_TOKEN_SECTRET": "secret",
  "SESSION_SECTRET": "secret",
  "REDISTOGO_URL": " ",
