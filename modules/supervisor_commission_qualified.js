/**
 * Created by emma on 8/26/15.
 * This function checks if a supervisor is qualified to receive the Shared Commission payment for the month.
 *
 * RULES THAT APPLY
 * 1) Each supervisor must have at least 1 sale in the current month in the sector o category being shared and for the
 *  "Telecos. & Energias" category a minimum of 2 sales is required
 */
var async                               = require('async');

module.exports = function(userid,Users, Product){

    //STEPS
    //1 get all supervisors
    //2 filter them buy category and check if they have sold any product based on the date and month
    //
/*
    Users.find({supervisor:true}, 'first_name last_name supervisor',function(err, supervisors){
        //console.log(supervisors);
    });

    Product.find({}, 'sector contract_state created',function(err, products){
        //console.log(products);
    });
*/
  return  async.parallel({

        /*Check for Telecos. & Energias sector */
        telecomQualified: function(callback){
            Product.aggregate([
                {$match:{
                    sector: 'Telecomunicación & Energías'
                }},
                {$match:{'contract_state_history.updated': function() {
                    var currentMonth = new Date().getMonth() + 1;
                    /* &&   17  <=  (new Date(this.contract_state_history.updated).getDate()) */
                    return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                }}},
                {$match:{
                    'contract_state_history.state': 'Comisionable'
                }},
                {$match:{
                    supervisor_info: function(){
                        return this._id == userid
                    }
                }},
                {$group: {
                    _id: '$sector',
                    productsSoldCount: {$sum: 1}
                }}
            ],function(err, telecomResults){
                if(!err){
                    callback(err, telecomResults);
                    //console.log(telecomResults);
                    return telecomResults;
                } else {
                    console.log(err);
                }
            })
        }


    });

   // console.log('All well');
};