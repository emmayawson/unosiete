var moment       = require('moment');

module.exports = {
        isAuthorized: function isAuthorized(req, res, next){
        if(!req.headers.authorization){
            //return res.status(401).send({error: 'Inicie sesión por favor!'})
            //console.log('Not authorized');
            res.status(401).send({error: 'Inicie sesión'});
            return
        }
        // console.log(req.headers.authorization);
        next();
    },

    //Check if the current month and place them in one of the 4 quarters of the year
    yearQuarter: function yearQuarter(){
        var currentMonth = parseInt(moment().format('MM')); //current month
        var quarter ;
        if(currentMonth === 1 && currentMonth <= 3){
            quarter = 'Primer quarto de año';
            console.log('1 quarter');
        } else if(currentMonth === 4 && currentMonth <= 6){
            quarter = 'Segundo quarto de año';
            console.log('2 quarter');
        } else if(currentMonth === 7 && currentMonth <= 9){
            quarter = 'Tercer quarto de año';
            console.log('3 quarter');
        }else if (currentMonth === 10 && currentMonth <= 11) {
            quarter = 'Último quarto de año';
            console.log('4 quarter');
        }

        return quarter;

    }

};