//config
var config = require('./config/development.js');

var express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');
var path = require('path');
var logger = require('morgan');
var multer = require('multer');
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt-nodejs');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var sessions = require('client-sessions');
var flash = require('express-flash');
var session = require('express-session');
var redirect = require("express-redirect");
var mongoose = require('mongoose');
var objectId = mongoose.Types.ObjectId; //For creation mongodb object id on the fly
var csrf = require('csurf');
var trim = require('trim');
var _ = require('lodash');
var async = require('async');
var accounting = require('accounting');
var formidable = require('formidable');
var randomString = require('random-string');
//var faker                 = require('faker');
var moment = require('moment');
var uuid = require('node-uuid');
var redis = require("redis");
var redisUrl = process.env.REDISTOGO_URL || config.REDISTOGO_URL;
var rtg = require("url").parse(redisUrl);
var redisStore = require('connect-redis')(session); //For redis session store
var client = redis.createClient(rtg.port, rtg.hostname);
var redisAuth = rtg.auth.split(":");
client.auth(redisAuth[1]);
var cloudinary = require('cloudinary');
var handlebars = require('handlebars');
var nodemailer = require('nodemailer');
var smtpapi = require('smtpapi');
var emailTemplate = require('email-templates').EmailTemplate;
var sendgrid = require("sendgrid")('unosiete', 'unosiete1234');


//Accounting JS config


//Cloudinary
cloudinary.config({
    cloud_name: 'unosiete',
    api_key: '738269378849656',
    api_secret: 'NOpUOEeTWnOmFMvMOZIJKRP6US0'
});


//Modules
var sendMail = require('./modules/sendmail.js'); //Send email
var qualifiedForCommission = require('./modules/supervisor_commission_qualified.js');


//models
var UserModel = require('./models/user.js');
var TelecommunicationModel = require('./models/telecommunication.js');
var InsuranceModel = require('./models/insurance.js');
var InvestmentModel = require('./models/investment.js');
var SmallBusinessModel = require('./models/small_business.js');
var MaintenanceModel = require('./models/maintenance.js');
var EcommerceModel = require('./models/ecommerce.js');
var CosmeticsModel = require('./models/cosmetics.js');
var PasswordChange = require('./models/password_change.js');
var Product = require('./models/product.js');
var ProductHistory = require('./models/product_history.js');
var PersonalCommissionNotifications = require('./models/personal_commission_notifications.js');
//var User      =   new UserModel;
//console.log(Telecommunication);


//Setup
var app = express();
var port = process.env.PORT || 3000;
var router = express.Router();
app.locals.basedir = path.join(__dirname, 'views');
app.locals.currentYear = new Date().getFullYear(); //Displayed in footer
app.locals.moment = moment; //for formating dates in the views

//Middleware
//redirect(app);
app.use('/', router);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.set('view engine', 'pug');
//app.use(logger('dev'));

//app.use(multer({dest:'./uploads/', inMemory: true}).single('uploadedFile'));
//var multerUploader = multer({ dest: 'uploads/' });
//app.use(multerUploader.array('uploadedFiles', 10));
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(sessions({
    cookieName: 'session', // cookie name dictates the key name added to the request object
    secret: 'blargadeeblargblarg', // should be a large unguessable string
    duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms
    activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
}));

app.use(session({
        secret: config.SESSION_SECTRET,
        // create new redis store.
        store: new redisStore({host: rtg.hostname, port: rtg.port, client: client}),
        /*cookie: { maxAge: 60000 , secure:true},*/
        resave: false,
        saveUninitialized: false
    }
));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));


//Env config
//console.log(process.env);
console.log(app.get('env'));
console.log(process.env.APP_ENV);
if (app.get('env') === 'development' || process.env.APP_ENV === 'staging' || process.env.APP_ENV === 'development') {
    app.locals.pretty = true;
    mongoose.set('debug', true);

    var conn = mongoose.connect(config.DB_URL_DEVELOPMENT).connection;
    //var conn = mongoose.connect(config.DB_PROD ).connection;
    //Use this to close database in case of errors
    function connectDB() {
        mongoose.connect(config.DB_URL_DEVELOPMENT).connection;
    };
    conn.on('error', function (err) {
        if (err) {//reconnect if error
            mongoose.disconnect();
            connectDB();
            console.log(err.message);
        }

    });
    conn.once('open', function () {
        console.log("mongodb connection open");
    });

} else {
    //mongoose.connect(config.DB_URL_DEVELOPMENT) ;
    //mongoose.connect(config.DB_URL_PRODUCTION, { autoIndex: false }) ;
    var conn = mongoose.connect(config.DB_UNOSIETE_LIVE, {autoIndex: false}).connection;

    function connectDBPROD() {
        mongoose.connect(config.DB_UNOSIETE_LIVE, {autoIndex: false}).connection;
    };
    conn.on('error', function (err) {
        //reconnect if error
        if (err) {
            mongoose.disconnect();
            connectDBPROD();
            console.log(err.message);
        }
    });
    conn.once('open', function () {
        console.log("mongodb connection open");
    });
}


app.locals.user = {};


//Check for the users session
app.use(function (req, res, next) {
    if (req.session && req.session.user) {
        req.user = req.session.user;
        res.locals.user = req.session.user;
        next();

    } else {
        next();

    }

});


function requireLoginAdmin(req, res, next) {


    if (req.user && req.user.admin === false) {
        res.locals.user = null;
        res.session.destroy();

        req.flash('error', 'Su cuenta no tiene los previlegios de administrador !');
        //req.method = 'get';
        res.redirect(301, '/admin/login');
    } else {
        next();
    }


}


function isAdmin(req, res, next) {
    if ((req.session) && (req.session.user.admin == false)) {
        console.log(req.session.user.admin);
        res.locals.user = null;
        res.session.destroy();

        //req.flash('error', 'Su cuenta no tiene los previlegios de administrador !');
        //res.render('./admin/login');
        //req.method = 'get';
        res.redirect(301, '/admin/login');

    } else {
        next();
    }


}


//Require supervisor login
function requireLoginSupervisor(req, res, next) {
    if (!req.user) {
        req.flash('error', 'Inicie session por favor!');
        res.redirect(301, '/supervisor/login');
    } else {
        next();
    }


}


//Check if user is admin o supervisor
function isSupervisor(req, res, next) {

    if ((req.user && req.user.supervisor === false)) {
        res.locals.user = null;
        req.session.destroy();
        req.flash('error', 'Su cuenta no tiene los previlegios de asesor!');
        res.redirect(301, '/supervisor/login');
    } else {
        next();
    }


}


//Check and display product notifications for the current supervisor
app.use(function (req, res, next) {
    if (req.session && req.session.user) {
        //console.log('User ID : ' + req.session && req.session.user._id);

        Product.find()
            .where('supervisor_info').equals(req.session.user._id)
            .where('product_notifications.supervisor_seen').equals(false)
            .populate('client_info')
            .exec(function (err, product) {

                if ((product[0] && product.length > 0) && !err) {
                    app.locals.productNotification = product;
                    app.locals.productNotification.messageCount = product.length;
                    next();
                } else {

                    //app.locals.productNotification.messageCount = null ;
                    app.locals.productNotification = null;

                    next(err);
                }
            });

    } else {
        next();
    }

});


//Check and display product notifications for the current all admins
app.use(function (req, res, next) {
    if (req.session && req.session.user) {


        Product.find()
            .where('product_notifications.admin_seen').equals(false)
            .populate('client_info')
            .exec(function (err, product) {
                //console.log(product);
                //console.log( 'MSG count: ' + product.length);
                if (product[0] && product.length > 0) {
                    app.locals.adminProductNotification = product;
                    app.locals.adminProductNotification.messageCount = product.length;
                    next();
                } else {

                    //app.locals.productNotification.messageCount = null ;
                    app.locals.adminProductNotification = null;

                    next();
                }
            });

    } else {
        next();
    }

});


//Check and display in admin supervisor personal commission request notifications
app.use(function (req, res, next) {

    if (req.session && req.session.user) {
        PersonalCommissionNotifications.find()
            .where('admin_seen').equals(false)
            .populate('requested_by', 'first_name last_name')
            .exec(function (err, notifications) {
                if (!err) {

                    app.locals.adminPersonalCommissionRequest = notifications;
                    app.locals.adminPersonalCommissionRequest.messagesCount = notifications.length;
                    next();
                } else {
                    next(err);
                }

            });


    } else {
        next();
    }
});

//Redirect the user to the dashboard if he is already logged in
app.use(function (req, res, next) {

    /*
     if(req.session && req.session.user.supervisor === true){
     next();
     }
     */
    next();
});


app.get('/', function (req, res) {
    res.render('./home');
});

app.get('/admin/login', function (req, res) {
    res.render('./admin/login');
});

app.post('/admin/login', function (req, res, next) {

    var user = req.body;
    //console.log(req.body);

    if (trim(user.email) == '' || trim(user.password) == '') {
        var msg = req.flash('info', 'Email/contraseña requerido para inicar sesion');
        //console.log(msg);
        res.render('./admin/login', {message: msg});
        res.end();
        return;
    }


    UserModel.findOne({email: user.email}, function (err, userDoc) {
        if (err) {
            return next(err);
        }

        //console.log(userDoc);
        if (userDoc) {
            bcrypt.compare(user.password, userDoc.password, function (err, results) {

                if (err) {
                    return next(err);
                }

                if (results) {
                    var payload = {
                        iss: req.hostname, //issuer
                        sub: userDoc._id     //user id
                    };


                    req.session.user = userDoc;
                    app.locals.user = userDoc;


                    res.redirect(301, '/admin/dashboard');
                } else {
                    var msg = req.flash('info', 'Contraseña no es correcto');
                    res.render('./admin/login', {message: msg});
                    res.end();
                }

            });

        } else {
            var msg = req.flash('info', 'Los datos introducidos son erroneos');
            res.render('./admin/login', {message: msg});
            res.end();
        }


    });


});

app.get('/admin/logout', requireLoginAdmin, function (req, res) {
    app.locals.user = null;
    req.session.destroy();
    res.redirect('/admin/login');
});

app.get('/admin/signup', function (req, res) {
    res.render('./admin/signup');
});

app.post('/admin/signup', function (req, res, next) {

    //console.log(req.body);
    var user = req.body;
    //console.log(user);
    if (!trim(user.first_name) || !trim(user.last_name) || !trim(user.email) || !trim(user.password) || !trim(user.password_confirm)) {
        var msg = req.flash('error', 'Todos los campos son obligatorios');
        res.render('./admin/signup');
        //res.end();
    }

    else if (user.password !== '' && user.password !== user.password_confirm) {
        var msg = req.flash('error', 'Sus contraseñas no coinciden!');
        res.render('./admin/signup');
        //res.end();
    } else {

        UserModel.count({email: user.email}, function (err, emailCount) {
            if (err) {
                return next(err);
            }
            //console.log(emailCount);
            if (emailCount === 1) {
                var msg = req.flash('error', 'Este email ya esta registrado!');
                res.render('./admin/signup');
                res.end();

            } else {
                var User = new UserModel({
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    password: user.password,
                    admin: true,
                    authorized: true,
                    supervisor: true
                });

                User.save(function (err) {
                    if (err) {

                        var msg = req.flash('error', 'Hubo un error, pruebalo mas tarde !');
                        res.render('./admin/signup');
                        res.end();

                    } else {

                        var msg = req.flash('success', 'Cuenta creada!');
                        res.redirect('/admin/login');


                    }
                });
            }
        });
    }


});

//ADMIN: dashboard
app.get('/admin/dashboard', requireLoginAdmin, function (req, res, next) {
    //TODO Remove activo and No activo


    Product.find({
            $or: [
                {contract_state: 'enviado'},
                {contract_state: 'Enviado'},
                {contract_state: 'Gestionado'},
                {contract_state: 'Incidencia'},
                {contract_state: 'Comisionable'},
                {contract_state: 'Activo'},
                {contract_state: 'No activo'},
                {contract_state: 'Tramitado'}
            ]
        })
        .sort({'_id': 'desc'})
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        /*.sort({created: 'asc'}) */
        .exec(function (err, products) {

            if (!err) {

                UserModel.find()
                    .where('supervisor').equals(true)
                    .select('first_name last_name identification_number')
                    .exec(function (err, users) {
                        if (!err) {
                            res.render('./admin/dashboard/index', {
                                products: products,
                                supervisors: users
                            });
                        } else {
                            return next(err);
                        }

                    });


            } else {
                return next(err);
            }
        });


});

//ADMIN: validate new signed contract
app.get('/admin/dashboard/new-contract-details/:id', requireLoginAdmin, function (req, res, next) {

    Product.findOne({})
        .where('_id').equals(req.params.id)
        .populate({path: 'supervisor_info '})
        .populate({path: 'client_info '})
        .populate({path: 'pro_supervisor_info '})
        .exec(function (err, product) {
            //console.log(product);
            if (!err) {
                //console.log(products);
                res.render('./admin/dashboard/production/index', {
                    product: product,
                    heading: 'Detalles producto en tramite'
                });

            } else {
                return next(err);
            }
        });


});


//ADMIN: Update product contact state and Insert new entry into the state history

app.post('/admin/products/update-product-state', requireLoginAdmin, function (req, res) {
    if (req.body.proSupervisorCommission == '') {
        req.body.proSupervisorCommission = 0;
    }

    if (req.body.proSupervisorId == '') {
        //Because the model has a reference to the User model it is obligatory
        //to provide a user id/mongoose object id else the update won't work
        req.body.proSupervisorId = mongoose.Types.ObjectId(); //Auto generate an object id

    }
    var item = req.body;
    console.log(item);


    if (req.body.contractState == 'Comisionable') {

        Product.findByIdAndUpdate(req.body.product,
            {
                $set: {
                    contract_state: req.body.contractState,
                    personal_commission: req.body.personalCommision,
                    shared_commission: req.body.sharedCommision,
                    pro_supervisor_commission: req.body.proSupervisorCommission,
                    pro_supervisor_info: req.body.proSupervisorId,
                    payment_received_date: new Date()
                }
            },
            function (err, product) {

                // Save the product state change to the product history
                Product.findOne({_id: req.body.product}, function (err, product) {
                    if (!err) {
                        product.product_notifications.push({
                            state: item.contractState,
                            created_by: req.session.user._id,
                            remarks: 'El producto ha cambiado de estado a ' + item.contractState + ' '
                        });


                        product.save(function (err) {
                            if (!err) {
                                res.json({message: 'OK'});
                            } else {

                                res.json({message: 'error'});
                                res.end();


                            }
                        });
                    } else {
                        res.json({error: 'Producto no encontrado'});
                        res.end();
                    }
                });


            });


    } else if (req.body.contractState === 'Incidencia') {
        //console.log(req.body);
        Product.update({_id: req.body.productId},
            {
                $set: {
                    contract_state: req.body.contractState
                }
            },
            function (err, doc) {

                if (!err) {

                    // Save the product state change to the product history

                    Product.findOne({_id: req.body.productId}, function (err, product) {
                        // console.log(product);
                        product.product_notifications.push({
                            state: req.body.contractState,
                            created_by: req.session.user._id,
                            remarks: 'El producto ha cambiado de estado a ' + req.body.contractState + ' '
                        });


                        product.save(function (err) {
                            if (!err) {
                                return res.json({message: 'OK', redirectToNotificationPage: true, product: product});

                            } else {
                                return res.json({error: 'Error recuperando los datos para redirecionar a la pagina de incidencias'});
                            }
                        });
                    });


                }

            });

    } else {

        Product.update({_id: req.body.productId},
            {
                $set: {
                    contract_state: req.body.contractState
                }
            }
            ,
            function (err) {

                // Save the product state change to the product history
                Product.findOne({_id: req.body.productId}, function (err, product) {
                    if (!err) {
                        product.product_notifications.push({
                            state: req.body.contractState,
                            created_by: req.session.user._id,
                            remarks: 'El producto ha cambiado de estado a ' + req.body.contractState + ' '
                        });


                        product.save(function (err) {
                            if (!err) {
                                res.json({message: 'OK'});
                            } else {

                                res.json({message: 'error'});
                                res.end();

                            }
                        });
                    } else {
                        res.json({error: 'Producto no encontrado'});
                        res.end();
                    }
                });


            });
    }


});


app.get('/admin/telecommunication/index', requireLoginAdmin, function (req, res, next) {
    TelecommunicationModel.find()
        .sort({product: 'asc'})
        .exec(function (err, telecomProducts) {
            //console.log(telecomProducts);
            if (!err) {

                res.render('./admin/dashboard/telecommunication/index',
                    {
                        heading: 'Productos sector telecos & energía',
                        telecomProducts: telecomProducts
                    });

            } else {
                return next(err);
            }

        });

});

app.get('/admin/telecommunication/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/telecommunication/new', {heading: 'Productos sector telecos & energía'});
});

app.post('/admin/telecommunication/new-product', requireLoginAdmin, function (req, res) {
    var item = req.body;
    var TelecommunicationProduct = new TelecommunicationModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    TelecommunicationProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});
            //return next(err);
        }
        else {
            res.send({message: 'Producto guardado correctamente!'});
        }
    });

});

app.get('/admin/telecommunication/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;

    TelecommunicationModel.find({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }
        res.render('./admin/dashboard/telecommunication/edit',
            {
                heading: 'Productos sector telecos & energía',
                editProduct: editProduct
            });
    });


});

app.put('/admin/telecommunication/edit/:id', requireLoginAdmin, function (req, res) {
    var item = req.body;
    TelecommunicationModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {

            if (err) {
                res.send({message: 'Error Producto no actualizado!'});

            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });

});

app.get('/admin/telecommunication/contracts', requireLoginAdmin, function (req, res) {
    //Todo change this model to get the data from contracts signed by Advisors(Asesores)
    TelecommunicationModel.find({}, function (err, telecomContracts) {

        Product.find()
            .where('sector').equals('Telecomunicación & Energías')
            .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
            .populate({path: 'supervisor_info ', select: 'first_name last_name'})
            .populate({path: 'client_info ', select: 'first_name last_name'})
            .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
            .exec(function (err, products) {
                console.log(products);
                if (!err) {
                    //console.log(products);
                    res.render('./admin/dashboard/telecommunication/contracts/index', {
                        products: products
                    });

                } else {
                    return next(err);
                }
            });


    });
});

app.get('/admin/telecommunication/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Telecomunicación & Energías')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/telecommunication/contracts/contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


app.get('/admin/telecommunication/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Telecomunicación & Energías')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/telecommunication/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});

//Seguros generales
app.get('/admin/insurance/index', requireLoginAdmin, function (req, res, next) {

    InsuranceModel.find()
        .sort({product: 'asc'})
        .exec(function (err, insuranceProducts) {
            //console.log(telecomProducts);
            if (!err) {

                res.render('./admin/dashboard/insurance/index',
                    {
                        heading: 'Productos sector telecos & energía',
                        insuranceProducts: insuranceProducts
                    });

            } else {
                return next(err);
            }

        });

});

app.get('/admin/insurance/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/insurance/new', {heading: 'Alta nuevo producto'});
});

app.post('/admin/insurance/new-product', function (req, res) {
    var item = req.body;
    var InsuranceProduct = new InsuranceModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    InsuranceProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});
            //return next(err);
        }
        else {
            res.send({message: 'Producto guardado correctamente!'});

        }
    });
});

app.get('/admin/insurance/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;
    InsuranceModel.find({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }

        res.render('./admin/dashboard/insurance/edit', {
            heading: 'Modificación de producto',
            editProduct: editProduct
        });
    });

});

app.put('/admin/insurance/edit/:id', function (req, res) {
    var item = req.body;
    InsuranceModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {


            if (err) {
                res.send({message: 'Error Producto no actualizado!'});
                //return next(err);
            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });
});

app.get('/admin/insurance/contracts', requireLoginAdmin, function (req, res, next) {


    Product.find({})
        .where('sector').equals('Seguros  Generales')
        .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {

            if (!err) {
                res.render('./admin/dashboard/insurance/contracts/index', {
                    products: products
                });

            } else {
                return next(err);
            }
        });

});

app.get('/admin/insurance/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Seguros  Generales')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/insurance/contracts/contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


app.get('/admin/insurance/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Seguros  Generales')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/insurance/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


//Investment
app.get('/admin/investment/index', requireLoginAdmin, function (req, res, next) {

    InsuranceModel.find()
        .sort({product: 'asc'})
        .exec(function (err, investmentContracts) {
            if (err) {
                return next(err);
            }
            res.render('./admin/dashboard/investment/index',
                {
                    heading: 'Productos del sector ahorro & inversión',
                    investmentContracts: investmentContracts
                });
        });

});

app.get('/admin/investment/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/investment/new', {heading: 'Alta nuevo producto'});
});

app.post('/admin/investment/new-product', function (req, res, next) {
    var item = req.body;
    var InvestmentProduct = new InvestmentModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    InvestmentProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});
        }
        else {
            res.send({message: 'Producto guardado correctamente!'});

        }
    });
});

app.get('/admin/investment/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;
    InvestmentModel.find({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }

        res.render('./admin/dashboard/investment/edit', {
            heading: 'Modificación de producto',
            editProduct: editProduct
        });
    });

});

app.put('/admin/investment/edit/:id', function (req, res) {
    var item = req.body;
    InvestmentModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {

            if (err) {
                res.send({message: 'Error Producto no actualizado!'});

            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });
});

app.get('/admin/investment/contracts', requireLoginAdmin, function (req, res, next) {


    Product.find({})
        .where('sector').equals('Ahorro & Inversión')
        .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            //console.log(products);
            if (!err) {

                res.render('./admin/dashboard/investment/contracts/index', {
                    products: products
                });

            } else {
                return next(err);
            }
        });


});

app.get('/admin/investment/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Ahorro & Inversión')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/investment/contracts/contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


app.get('/admin/investment/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Ahorro & Inversión')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/investment/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


//Small business (Ahorro gestion pymes)

app.get('/admin/small-business/index', requireLoginAdmin, function (req, res, next) {
    SmallBusinessModel.find()
        .sort({product: 'asc'})
        .exec(function (err, smallBusinessContracts) {
            if (err) {
                return next(err);
            }
            res.render('./admin/dashboard/small_business/index',
                {
                    heading: 'Productos ahorro autónomos y empresa',
                    smallBusinessContracts: smallBusinessContracts
                });
        });


});

app.get('/admin/small-business/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/small_business/new', {heading: 'Alta nuevo producto'});
});

app.post('/admin/small-business/new-product', function (req, res) {
    var item = req.body;
    var SmallBusinessProduct = new SmallBusinessModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    SmallBusinessProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});
            //return next(err);
        }
        else {
            res.send({message: 'Producto guardado correctamente!'});

        }
    });
});

app.get('/admin/small-business/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;
    SmallBusinessModel.find({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }

        res.render('./admin/dashboard/small_business/edit', {
            heading: 'Modificación de producto',
            editProduct: editProduct
        });
    });

});

app.put('/admin/small-business/edit/:id', function (req, res) {
    var item = req.body;
    SmallBusinessModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {

            if (err) {
                res.send({message: 'Error Producto no actualizado!'});
                //return next(err);
            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });
});

app.get('/admin/small-business/contracts', requireLoginAdmin, function (req, res, next) {


    Product.find({})
        .where('sector').equals('Autónomos & Empresa')
        .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {

            if (!err) {
                res.render('./admin/dashboard/small_business/contracts/index', {
                    products: products
                });

            } else {
                return next(err);
            }
        });


});

app.get('/admin/small-business/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Autónomos & Empresa')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/small_business/contracts/contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


app.get('/admin/small-business/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Autónomos & Empresa')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/small_business/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});

//Maintenanace (Manteniminento y reparaciones)

app.get('/admin/maintenance/index', requireLoginAdmin, function (req, res, next) {

    MaintenanceModel.find()
        .sort({product: 'asc'})
        .exec(function (err, maintenanceProducts) {

            if (err) {
                return next(err);
            }

            res.render('./admin/dashboard/maintenance/index',
                {
                    heading: 'Productos del mantenimiento y reparaciones',
                    maintenanceProducts: maintenanceProducts
                });
        });


});

app.get('/admin/maintenance/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/maintenance/new', {heading: 'Alta nuevo producto'});
});

app.post('/admin/maintenance/new-product', function (req, res) {
    var item = req.body;
    var MaintenanceProduct = new MaintenanceModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    MaintenanceProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});

        }
        else {
            res.send({message: 'Producto guardado correctamente!'});


        }
    });
});

app.get('/admin/maintenance/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;
    MaintenanceModel.find({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }

        res.render('./admin/dashboard/maintenance/edit', {
            heading: 'Modificación de producto',
            editProduct: editProduct
        });
    });

});

app.put('/admin/maintenance/edit/:id', function (req, res) {
    var item = req.body;
    MaintenanceModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {

            //console.log(editedProduct);
            if (err) {
                res.send({message: 'Error, Producto no actualizado!'});
                //return next(err);
            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });
});

app.get('/admin/maintenance/contracts', requireLoginAdmin, function (req, res, next) {

    Product.find({})
        .where('sector').equals('Mantenimiento & Reparaciones')
        .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {

            if (!err) {

                res.render('./admin/dashboard/maintenance/contracts/index', {
                    products: products
                });

            } else {
                return next(err);
            }
        });


});

app.get('/admin/maintenance/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Mantenimiento & Reparaciones')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/maintenance/contracts/contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


app.get('/admin/maintenance/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Mantenimiento & Reparaciones')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/maintenance/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err)
            }
        });

});


//E-Commerce (Alimentacion y e-comerces)
app.get('/admin/ecommerce/index', requireLoginAdmin, function (req, res, next) {

    EcommerceModel.find()
        .sort({product: 'asc'})
        .exec(function (err, ecommerce) {

            if (err) {
                return next(err);
            }

            res.render('./admin/dashboard/ecommerce/index',
                {
                    heading: 'Productos de alimenteción y e-commerce',
                    ecommerce: ecommerce
                });
        });


});

app.get('/admin/ecommerce/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/ecommerce/new', {heading: 'Alta nuevo producto'});
});

app.post('/admin/ecommerce/new-product', function (req, res) {
    var item = req.body;

    var EcommerceProduct = new EcommerceModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    EcommerceProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});
            //return next(err);
        }
        else {
            res.send({message: 'Producto guardado correctamente!'});

        }
    });
});

app.get('/admin/ecommerce/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;
    EcommerceModel.find({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }

        res.render('./admin/dashboard/ecommerce/edit', {
            heading: 'Modificación de producto',
            editProduct: editProduct
        });
    });

});

app.put('/admin/ecommerce/edit/:id', function (req, res) {
    var item = req.body;
    //console.log(item);
    EcommerceModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {

            //console.log(editedProduct);
            if (err) {
                res.send({message: 'Error Producto no actualizado!'});
                //return next(err);
            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });
});

app.get('/admin/ecommerce/contracts', requireLoginAdmin, function (req, res, next) {


    Product.find({})
        .where('sector').equals('Alimentación & E-commerce')
        .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            //console.log(products);
            if (!err) {
                //console.log(products);
                res.render('./admin/dashboard/ecommerce/contracts/index', {
                    products: products
                });

            } else {
                return next(err);
            }
        });


});

app.get('/admin/ecommerce/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Alimentación & E-commerce')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/ecommerce/contracts/contracted', {
                    products: products
                });
            } else {
                next(err);
            }
        });

});


app.get('/admin/ecommerce/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Alimentación & E-commerce')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/ecommerce/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err);
            }
        });

});


//cosmetics (Bienestar y cosmetica)

app.get('/admin/cosmetics/index', requireLoginAdmin, function (req, res, next) {

    CosmeticsModel.find()
        .sort({product: 'asc'})
        .exec(function (err, cosmeticsContracts) {

            if (err) {
                return next(err);
            }

            res.render('./admin/dashboard/cosmetics/index',
                {
                    heading: 'Productos sector bienestar &  cosmetica',
                    cosmeticsContracts: cosmeticsContracts
                });
        });


});

app.get('/admin/cosmetics/new-product', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/cosmetics/new', {heading: 'Alta nuevo producto'});
});

app.post('/admin/cosmetics/new-product', function (req, res) {
    var item = req.body;
    var CosmeticsProduct = new CosmeticsModel({
        provider: item.provider,
        provider_email: item.email,
        product: item.product,
        points: item.points
    });

    CosmeticsProduct.save(function (err) {
        if (err) {
            res.send({message: 'Error, el producto no se guardó. Intentelo de nuevo  mas tarde.'});
        }
        else {
            res.send({message: 'Producto guardado correctamente!'});

        }
    });
});

app.get('/admin/cosmetics/edit/:id', requireLoginAdmin, function (req, res, next) {
    var productId = req.params.id;
    CosmeticsModel.findOne({_id: productId}, function (err, editProduct) {

        if (err) {
            return next(err);
        }

        res.render('./admin/dashboard/cosmetics/edit', {
            heading: 'Modificación de producto',
            editProduct: editProduct
        });
    });

});

app.put('/admin/cosmetics/edit/:id', function (req, res) {
    var item = req.body;

    CosmeticsModel.update({_id: item.productId},
        {
            $set: {
                provider: item.provider,
                provider_email: item.email,
                product: item.product,
                points: item.points
            }
        }, function (err, editedProduct) {


            if (err) {
                res.send({message: 'Error Producto no actualizado!'});
                //return next(err);
            } else {
                res.send({message: 'Producto actualizado!'});
            }

        });
});

app.get('/admin/cosmetics/contracts', requireLoginAdmin, function (req, res, next) {

    Product.find()
        .where('sector').equals('Hogar & Bienestar')
        .where('contract_state').in(['Gestionado', 'Enviado', 'Incidencia', 'Comisionable'])
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {

            if (!err) {

                res.render('./admin/dashboard/cosmetics/contracts/index', {
                    products: products
                });

            } else {
                return next(err);
            }
        });


});

app.get('/admin/cosmetics/contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('Contratado')
        .where('sector').equals('Hogar & Bienestar')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/cosmetics/contracts/contracted', {
                    products: products
                });
            } else {
                next(err);
            }
        });

});


app.get('/admin/cosmetics/not-contracted', function (req, res, next) {
    Product.find()
        .where('contract_state').equals('No contratado')
        .where('sector').equals('Hogar & Bienestar')
        .populate({path: 'supervisor_info ', select: 'first_name last_name'})
        .populate({path: 'client_info ', select: 'first_name last_name'})
        .populate({path: 'pro_supervisor_info ', select: 'first_name last_name'})
        .exec(function (err, products) {
            if (!err) {
                res.render('./admin/dashboard/cosmetics/contracts/not-contracted', {
                    products: products
                });
            } else {
                next(err);
            }
        });

});


//Reports (Informes)
app.get('/admin/reports/index', requireLoginAdmin, function (req, res, next) {

    var results = [];
    var salesData = {};
    UserModel.find()
        .where('supervisor').equals(true)
        .exec(function (err, supervisorsList) {

            var ids = [];
            supervisorsList.forEach(function (items) {
                ids.push(items._id);
            });
            //Get sale for each supervisor

            Product.find()
                .where('supervisor_info').in(ids)
                .where('contract_state').in('Contratado')
                .exec(function (err, supervisorsProducts) {


                    if (!err) {
                        // console.log(supervisorsProducts.length);
                    } else {

                    }

                });
        });


    res.render('./admin/dashboard/reports/index', {heading: 'Informes '});
});


app.get('/admin/reports/shared-commissions', function (req, res, next) {


    function yearQuarter(startMonth, endMonth) {
        var currentMonth = parseInt(new Date().getMonth() + 1); //current month
        var sm = startMonth || 1;
        var em = endMonth || 3;

        return (new Date(this.payment_received_date).getMonth() + 1) == {$gt: sm, $lt: em};
    };

    async.parallel({
        telecom1Q: function (callback) { //Calculate shaerd commission for Jan - Mar
            //IMPORTANT: when working  with aggregates always filter by date first else it wont work properly
            Product.aggregate([

                {$match: {sector: 'Telecomunicación & Energías'}},
                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        telecom2Q: function (callback) { //Calculate sheared commission for Apr - Jun

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Telecomunicación & Energías'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        telecom3Q: function (callback) { //Calculate shared commission for Apr - Jun

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Telecomunicación & Energías'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        telecom4Q: function (callback) { //Calculate shared commission for Apr - Jun

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Telecomunicación & Energías'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        investment1Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Ahorro & Inversión'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        investment2Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Ahorro & Inversión'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        investment3Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Ahorro & Inversión'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        investment4Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Ahorro & Inversión'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        insurance1Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Seguros  Generales'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        insurance2Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Seguros  Generales'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        insurance3Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Seguros  Generales'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        insurance4Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Seguros  Generales'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        maintenance1Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Mantenimiento & Reparaciones'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        maintenance2Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Mantenimiento & Reparaciones'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        maintenance3Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Mantenimiento & Reparaciones'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        maintenance4Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Mantenimiento & Reparaciones'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        smallBusiness1Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Autónomos & Empresa'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        smallBusiness2Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Autónomos & Empresa'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        smallBusiness3Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Autónomos & Empresa'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        smallBusiness4Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Autónomos & Empresa'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },

        ecommerce1Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Alimentación & E-commerce'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        ecommerce2Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Alimentación & E-commerce'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        ecommerce3Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Alimentación & E-commerce'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        ecommerce4Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Alimentación & E-commerce'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },

        cosmetics1Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Hogar & Bienestar'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 1, $lte: 3}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        cosmetics2Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Hogar & Bienestar'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 4, $lte: 6}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        cosmetics3Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Hogar & Bienestar'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 7, $lte: 9}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },
        cosmetics4Q: function (callback) {

            Product.aggregate([

                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {$match: {sector: 'Hogar & Bienestar'}},
                {
                    $project: {
                        name: 1, shared_commission: 1, year: 1,
                        month: {$month: "$payment_received_date"},
                        year: {$year: "$payment_received_date"}
                    },

                },
                {$match: {month: {$gte: 10, $lte: 12}}},
                {
                    $group: {
                        _id: "$year",
                        total: {$sum: "$shared_commission"}
                    }
                }


            ], function (err, data) {
                if (!err) {
                    callback(null, data);
                } else {
                    callback(err, null);
                }
            });

        },

    }, function (err, results) {


        if (!err) {
            res.render('./admin/dashboard/reports/shared_commissions',
                {
                    results: results,
                    heading: 'Comisiones compartidas  trimestral'
                });

        } else {
            next(err);
        }

    });


});


//Supervisors (Asesores)
app.get('/admin/supervisors/index', requireLoginAdmin, function (req, res, next) {

    UserModel.find({supervisor: true}, function (err, supervisors) {
        if (!err) {
            res.render('./admin/dashboard/supervisors/index', {heading: 'Asesores', supervisors: supervisors});
        } else {
            return next(err);
        }
    });


});


app.get('/admin/supervisors/show/:id', function (req, res, next) {

    UserModel.findOne()
        .where('_id').equals(req.params.id)
        .populate({path: 'supervisor_recommendation_history.created_by', select: 'first_name last_name'})
        .populate({path: 'supervisor_recommendation_history.approved_by', select: 'first_name last_name'})
        .exec(function (err, supervisor) {
            if (!err) {
                res.render('./admin/dashboard/supervisors/show', {
                    heading: 'Perfil Asesor',
                    supervisor: supervisor, history: supervisor.supervisor_recommendation_history
                });
            } else {
                return next(err);
            }

        });

});


app.get('/admin/supervisors/new', requireLoginAdmin, function (req, res) {
    res.render('./admin/dashboard/supervisors/new', {heading: 'Alta nuevo asesor'});
});

app.post('/admin/supervisors/new', function (req, res, next) {

    var sup = req.body;


    //Check if the email exists
    UserModel.count({email: sup.email}, function (err, count) {
        if (!err) {
            if (count >= 1) {
                res.json({error: 'Este email ya esta registrado a otro usuario'});
                res.end();
            }
        } else {
            return next(err);
        }
        // return;
    });


    //Check if the DNI/NIE  exists
    UserModel.count({identification_number: sup.identification_number}, function (err, count) {
        if (!err) {
            if (count >= 1) {
                res.json({error: 'Este DNI/DIE ya esta registrado a otro usuario'});
                res.end();
            } else {
                var Supervisor = new UserModel({
                    first_name: sup.first_name,
                    last_name: sup.last_name,
                    identification_number: sup.identification_number,
                    email: sup.email,
                    telephone: sup.telephone,
                    mobile: sup.mobile,
                    address: sup.address,
                    city: sup.city,
                    post_code: sup.post_code,
                    date_of_birth: sup.date_of_birth,
                    supervisor: true,
                    created_by: req.session.user._id,
                    account_creation_state: {state: sup.account_creation_state, updated: new Date().toISOString()}
                });

                Supervisor.save(function (err) {
                    if (!err) {
                        res.json({message: 'Supervisor creado corectamente.'});
                        res.end();
                    } else {
                        res.json({message: 'Error, no se creó el nuevo acesor. Intentelo más tarde'});
                        res.end();
                    }
                });
            }
        } else {
            return next(err);
        }

    });


});

app.get('/admin/supervisors/edit/:id', requireLoginAdmin, function (req, res, next) {
    UserModel.findOne({_id: req.params.id}, function (err, supervisor) {
        if (!err) {
            //console.log(supervisor);
            res.render('./admin/dashboard/supervisors/edit',
                {heading: 'Modificar los datos de asesor', supervisor: supervisor});
        } else {
            return next(err);
        }

    });


});


app.put('/admin/supervisors/edit/:id', function (req, res) {
    var sup = req.body;

    UserModel.update({_id: req.params.id},
        {
            $set: {
                first_name: sup.first_name,
                last_name: sup.last_name,
                identification_number: sup.identification_number,
                email: sup.email,
                telephone: sup.telephone,
                mobile: sup.mobile,
                address: sup.address,
                city: sup.city,
                post_code: sup.post_code,
                date_of_birth: moment(sup.date_of_birth, 'DD/MM/YYYY').utc().format()
                /*account_creation_state: {state: sup.account_creation_state, updated: new Date().toISOString()}*/
            }
        }, function (err, supervisor) {
            if (!err) {
                //console.log(supervisor);
                return res.json({message: 'Asssor modificado corectamente'});
            } else {
                return res.json({error: 'Error, no se guardó los cambios, intentelo de nuevo maś tarde'});
            }

        });


});


app.delete('/admin/supervisors/edit/:id', function (req, res) {

    UserModel.remove({_id: req.params.id}, function (err, supervisor) {
        if (!err) {
            //console.log(supervisor);
            return res.json({message: 'OK'});
        } else {
            return res.json({error: 'Error'});
        }

    });

});


//Authorize supervisor account
app.post('/admin/supervisor/authorize-account', function (req, res) {


//TODO check if the req.body.status === true then acitvate account else send account deactivation email to user to
    //console.log(req.get('host'));
    //console.log(req.get('origin'));
    UserModel.findOne({_id: req.body.userid}, function (err, supervisor) {
        var emailData = {};
        emailData.templatePath = './templates/welcome_supervisor.hbs';
        emailData.subject = 'Bienvenido a Unosiete.us';
        //emailData.to                    =   'emyawson@gmail.com';
        emailData.to = supervisor.email;
        emailData.from = 'nosotros@unosiete.us';
        emailData.fromname = 'Administracion Unosiete';
        emailData.username = supervisor.first_name + ' ' + supervisor.last_name;
        emailData.activationCode = uuid.v1();
        emailData.domain = req.get('origin');
        //emailData.userid                  =   supervisor._id;
        //console.log(supervisor);
        if (emailData.activationCode) {
            var changePass = new PasswordChange({hash: emailData.activationCode, user_id: supervisor._id});
            changePass.save(function (err) {
                if (err) {
                    return res.json({error: 'Error en la activación de la cuenta del asesor'});

                } else {

                    UserModel.update({_id: req.body.userid},
                        {$set: {authorized: req.body.status}}, function (err, supervisor) {
                            if (!err) {
                                //Send welcome email to supervisor
                                sendMail(emailData);
                                return res.json({message: 'OK'});
                            } else {
                                return res.json({error: 'Error al actualizar el estado de la cuenta'});
                            }

                        });

                }
            });
        }

    });


});

app.post('/admin/supervisor/approve-supervisor', requireLoginAdmin, function (req, res) {

    //console.log(req.body);
    if (req.body.status == 'true') {
        UserModel.findOne()
            .where('_id').equals(req.body.userid)
            .exec(function (err, supervisor) {
                supervisor.supervisor_recommendation_history.push({
                    state: 'validado',
                    approved_by: req.session.user._id
                });
                //supervisor.authorized = true;

                supervisor.save(function (err, supervisor) {
                    if (!err) {
                        //console.log(supervisor);
                        return res.json({message: 'OK'});

                    } else {
                        return res.json({error: 'Error'});

                    }

                });
            });
    } else {
        //console.log(req.body);
    }

});

//Recommended supervisors
app.get('/admin/supervisor/recomended', requireLoginAdmin, function (req, res, next) {

    UserModel.find({supervisor: true, authorized: false}, function (err, supervisors) {

        if (!err) {

            res.render('./admin/dashboard/supervisors/recomended', {supervisors: supervisors});
        } else {
            return next(err);
        }
    });

});


/**
 * ===============================================================
 *  Admin product notifications
 * ===============================================================
 */


app.get('/admin/product-notification', requireLoginAdmin, function (req, res) {

    res.render('./admin/dashboard/product_notifications/new', {
        productId: req.query.product_id,
        productState: req.query.product_state
    });
});

app.post('/admin/product-notification', function (req, res) {


    var newFileName = randomString({
        length: 12,
        numeric: true,
        letters: false,
        special: false
    });
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {

        if (!err) {
            if (req.session && req.session.user) {

                if (files && files.uploadedFile.name !== '') {


                    cloudinary.uploader.upload(files.uploadedFile.path, function (result) {

                        Product.findOne({_id: fields.product_id}, function (err, product) {

                            if (!err) {
                                product.product_notifications.push({
                                    state: fields.product_state,
                                    created_by: req.session.user._id,
                                    remarks: fields.observations,
                                    attachment_name: fields.fileName1 || false,
                                    attachment: result,
                                    admin_seen: true // This is to prevent the admin from seeing his owm notifications as not read
                                });


                                product.save(function (err) {
                                    if (!err) {
                                        res.json({message: 'OK'});
                                    } else {
                                        res.json({error: 'Error, no se creó la nueva notificación'});

                                    }
                                });
                            } else {
                                res.json({error: 'Error recuperando el producto con id' + fields.product_id});

                            }
                        })

                    }, {public_id: 'product_observations_attachments/' + newFileName, resource_types: 'raw'});

                } else {


                    Product.findOne({_id: fields.product_id}, function (err, product) {

                        if (!err) {
                            product.product_notifications.push({
                                state: fields.product_state,
                                created_by: req.session.user._id,
                                remarks: fields.observations,
                                admin_seen: true // This is to prevent the admin from seeing his owm notifications as not read
                            });


                            product.save(function (err) {
                                if (!err) {
                                    res.json({message: 'OK'});
                                } else {
                                    res.json({error: 'Error, no se creó la nueva notificación'});

                                }
                            });
                        } else {
                            res.json({error: 'Error recuperando el producto con id' + fields.product_id});

                        }
                    })

                }


            } else {
                res.json({info: 'login'});
                res.end();
            }
        } else {
            es.json({error: 'Error procesando el formulario'});
        }


    });


});

app.get('/admin/product-notification/show/:id', requireLoginAdmin, function (req, res, next) {

    Product.findOne()
        .where('_id').equals(req.params.id)
        .populate('product_notifications.created_by', 'first_name last_name')

        .exec(function (err, notifications) {


            _.forEach(notifications.product_notifications, function (item) {

                if (item.admin_seen == false) {
                    item.admin_seen = true;
                    notifications.save(function (err) {
                        if (err) {
                            return next(err);
                        } else {
                            //console.log('All ok');
                        }
                    });
                }


            });


            if (!err) {
                res.render('./admin/dashboard/product_notifications/show',
                    {notifications: notifications.product_notifications.reverse()});
            } else {
                return next(err);
            }
        });

});


/**
 * ===============================================================
 *  Admin delete product permanentely
 * ===============================================================
 */

app.post('/admin/product/delete/', function (req, res) {

    console.log(req.body);
    if (req.session && req.session.user) {

        Product
            .findByIdAndRemove(req.body.product_id, function (err) {
                if (!err) {
                    return res.json({message: 'OK'});
                } else {
                    return res.json({error: 'Error no podemos borrar este producto en este momento'});
                }
            });

    } else {
        return res.json({info: 'login'});
    }

});


/**
 * ===============================================================
 *  Admin suervisor personal commission request notifications
 * ===============================================================
 */

app.get('/admin/supervisor-personal-commission/show/:supervisor_id', function (req, res, next) {

    async.parallel({
        products: function (callback) {
            Product.find()
                .where('supervisor_info').equals(req.params.supervisor_id)
                .where('payment_received_date').ne(null)
                .where('personal_commission_payment').equals(false)
                .where('contract_state').equals('Contratado')
                .populate('client_info')
                .populate('pro_supervisor_info')
                .exec(function (err, products) {
                    if (!err) {
                        //console.log(products);
                        callback(err, products);
                    }

                });


        },
        personalCommission: function (callback) {
            Product.aggregate([
                {
                    $match: {
                        supervisor_info: new objectId(req.params.supervisor_id)
                    }
                },
                {
                    $match: {
                        payment_received_date: {$ne: null}
                    }
                },
                {
                    $match: {
                        personal_commission_payment: {$eq: false}
                    }
                },
                {
                    $match: {
                        contract_state: {$eq: 'Contratado'}
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalPersonalCommission: {$sum: '$personal_commission'}
                    }
                }

            ], function (err, data) {
                callback(err, data);
            });

        },
        proCommission: function (callback) {
            Product.aggregate([
                {
                    $match: {
                        supervisor_info: new objectId(req.session.user._id)
                    }
                },
                {
                    $match: {
                        payment_received_date: {$ne: null}
                    }
                },
                {
                    $match: {
                        personal_commission_payment: {$eq: false}
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalProCommission: {$sum: "$pro_supervisor_commission"}
                    }
                }


            ], function (err, data) {

                callback(err, data);

            });

        },

    }, function (err, results) {

        if (!err) {
            var products = (results.products.length > 0 ) ? results.products : {};
            var personalCommission = (results.personalCommission.length > 0) ? results.personalCommission[0].totalPersonalCommission : 0;
            var proCommission = (results.proCommission.length > 0) ? results.proCommission[0].totalProCommission : 0;
            res.render('./admin/dashboard/supervisor_personal_commission/show', {
                products: products,
                personalCommission: personalCommission,
                supervisor_id: req.params.supervisor_id,
                totalProCommission: proCommission
            });
        } else {
            next(err)
        }
    });

});

app.post('/admin/supervisor-personal-commission/pay', function (req, res, next) {


    var payment = {
        $set: {
            personal_commission_payment: {
                paid_by: req.body.admin,
                date: Date.now()
            }
        }
    };



    Product.update({supervisor_info: req.body.supervisor}, payment, {multi: true}, function (err) {
        if (!err) {

            console.log('Product Executed!');

            //Deactivate notification for this request
            PersonalCommissionNotifications.update({
                requested_by: req.body.supervisor, paid: false
            }, {
                $set: {
                    paid: true,
                    date_paid: Date.now(),
                    admin_seen: true
                }
            }, function (err) {
                if (!err) {
                    return res.json({message: 'OK'});

                } else {
                    return res.json({error: 'Error borrando la notificación de la solicitud actual'});

                }
            });


        } else {
            console.log(err);
            res.json({error: 'Error actualizando el estado de las comisiones'});

        }
    });


});


/**
 * ===============================================================
 *  Admin supervisor paid personal & pro commissions list
 * ===============================================================
 */

app.get('/admin/paid-commissions', function (req, res, next) {

    //Get list of supervisors
    UserModel.find()
        .where('supervisor').equals(true)
        .exec(function (err, supervisors) {
            if (!err) {
                res.render('./admin/dashboard/paid_commissions/index', {supervisors: supervisors});
            } else {
                return next(err);
            }


        });

});

app.get('/admin/paid-commissions-personal/:supervisor_id', function (req, res, next) {

    //Get list of supervisors

    Product.find()
        .where('supervisor_info').equals(req.params.supervisor_id)
        .where('personal_commission_payment.date').ne(null)
        .populate('client_info')
        .sort({created: -1})
        .exec(function (err, comisionedProducts) {

            if (!err) {
                res.render('./admin/dashboard/paid_commissions/personal',
                    {comisionedProducts: comisionedProducts});
            } else {
                return next(err);
            }

        });


});

app.get('/admin/paid-commissions-pro/:supervisor_id', function (req, res, next) {

    //Get list of supervisors

    Product.find()
        .where('pro_supervisor_info').equals(req.params.supervisor_id)
        .where('payment_received_date').ne(null)
        .where('contract_state').equals('Contratado')
        .populate('client_info')
        .sort({created: -1})
        .exec(function (err, products) {
            if (!err) {
                return res.render('./admin/dashboard/paid_commissions/pro', {products: products});
            } else {
                return next(err);
            }

        });


});

/**
 * ===============================================================
 *  Admin supervisor points of sold products
 * ===============================================================
 */
app.get('/admin/supervisor-points', function (req, res, next) {
    //Get list of supervisors
    UserModel.find()
        .where('supervisor').equals(true)
        .exec(function (err, supervisors) {

            if (!err) {
                res.render('./admin/dashboard/punctuations/index', {supervisors: supervisors});
            } else {
                return next(err);
            }

        });
});


app.get('/admin/supervisor-points/show/:supervisor_id', function (req, res, next) {
    //Get list of supervisors
    Product.find()
        .where('supervisor_info').equals(req.params.supervisor_id)
        .where('contract_state').equals('Contratado')
        .sort({created: -1})
        .exec(function (err, products) {

            if (!err) {
                res.render('./admin/dashboard/punctuations/show', {products: products});
            } else {
                return next(err);
            }

        });
});


/**
 * ===============================================================
 *  Supervisor(Asesor)
 * ===============================================================
 */



app.get('/supervisor/login', function (req, res) {
    res.render('./supervisor/login');

});

app.post('/supervisor/login', function (req, res, next) {
    var user = req.body;
    //console.log(user);

    if (trim(user.email) == '' || trim(user.password) == '') {
        var msg = req.flash('info', 'Email/contraseña requerido para inicar sesion');
        //console.log(msg);
        res.render('./supervisor/login', {message: msg});
        res.end();
        return;
    }


    User.findOne({email: user.email}, function (err, userDoc) {
        if (err) {
            return next(err);
        }

        else if (userDoc) {
            bcrypt.compare(user.password, userDoc.password, function (err, results) {

                if (err) {
                    return next(err);
                }

                if (results) {

                    if (userDoc.authorized !== false) {
                        req.user = userDoc;
                        delete req.user.password;
                        req.session.user = req.user;

                        res.redirect(301, '/supervisor/dashboard');

                    } else {
                        var msg = req.flash('error', 'Su cuenta aún no ha sido authorizado para poder utilizarse, ' +
                            'por favor pongase en contacto con su administrador para que te activen la cuenta!');
                        res.render('./supervisor/login');
                        res.end();
                    }


                } else {
                    var msg = req.flash('error', 'Contraseña no es correcto');
                    res.render('./supervisor/login');
                    res.end();
                }

            });

        } else {
            var msg = req.flash('error', 'Los datos introducidos son erroneos');
            res.render('./supervisor/login');
            res.end();
        }

    });
});

app.get('/supervisor/logout', function (req, res) {

    app.locals.user = null;
    res.locals.user = null;
    req.session.destroy();
    res.redirect(301, '/supervisor/login');
});

app.get('/supervisor/dashboard', requireLoginSupervisor, isSupervisor, function (req, res) {


    //Months to be used in the view to display the mont as string
    var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];

    var currentMonth = parseInt(moment(new Date()).format('MM'));
    var quarter;
    var check = {};


    if (req.session && req.session.user) {

        async.parallel({
            quarter_telecom: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth >= 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Telecomunicación & Energías'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionTelecom: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {

                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            },

            quarter_insurance: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth === 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Seguros  Generales'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionInsurance: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {

                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            },
            quarter_inversion: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth === 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Ahorro & Inversión'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionInversion: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            },
            quarter_maintenance: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth === 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Mantenimiento & Reparaciones'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionMaintenance: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {

                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            },
            quarter_small_business: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth === 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Autónomos & Empresa'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionSmallBusiness: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            },
            quarter_ecommerce: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth === 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Alimentación & E-commerce'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionEcommerce: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {

                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            },


            quarter_cosmetics: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            payment_received_date: function () {
                                var currentMonth = parseInt(new Date().getMonth() + 1); //current month
                                var quarter;
                                if (currentMonth >= 1 && currentMonth <= 3) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 3;
                                }
                                if (currentMonth >= 4 && currentMonth <= 6) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 6;
                                }
                                if (currentMonth >= 7 && currentMonth <= 9) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 9;
                                }

                                if (currentMonth === 10 && currentMonth <= 12) {
                                    return (new Date(this.payment_received_date).getMonth() + 1) == currentMonth && currentMonth <= 12;
                                }

                            }
                        }
                    },
                    {
                        $match: {
                            $or: [{contract_state: 'Comisionable'},
                                {contract_state: 'Contratado'}]
                        }
                    },
                    {
                        $match: {
                            sector: 'Hogar & Bienestar'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            sharedCommissionCosmetics: {$sum: '$shared_commission'}
                        }
                    }
                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);
                        return results;
                    } else {
                        return next(err);
                    }
                })
            }


        }, function (err, data) {

            //This part prevents errors if there are no values for the results. Zero will be shown to the user
            var sharedCommissionTelecom = (data.quarter_telecom.length > 0) ? accounting.toFixed(data.quarter_telecom[0].sharedCommissionTelecom, 2) : 0.00;
            var sharedCommissionInsurance = (data.quarter_insurance.length > 0) ? accounting.toFixed(data.quarter_insurance[0].sharedCommissionInsurance, 2) : 0.00;
            var sharedCommissionInversion = (data.quarter_inversion.length > 0) ? accounting.toFixed(data.quarter_inversion[0].sharedCommissionInsurance, 2) : 0.00;
            var sharedCommissionMaintenance = (data.quarter_maintenance.length > 0) ? accounting.toFixed(data.quarter_maintenance[0].sharedCommissionMaintenance, 2) : 0.00;
            var sharedCommissionSmallBusiness = (data.quarter_small_business.length > 0) ? accounting.toFixed(data.quarter_small_business[0].sharedCommissionSmallBusiness, 2) : 0.00;
            var sharedCommissionEcommerce = (data.quarter_ecommerce.length > 0) ? accounting.toFixed(data.quarter_ecommerce[0].sharedCommissionEcommerce, 2) : 0.00;
            var sharedCommissionCosmetics = (data.quarter_cosmetics.length > 0) ? accounting.toFixed(data.quarter_cosmetics[0].sharedCommissionCosmetics, 2) : 0.00;
            //console.log(sharedCommissionCosmetics);
            res.render('./supervisor/dashboard/index', {
                sharedCommissionTelecom: sharedCommissionTelecom,
                sharedCommissionInsurance: sharedCommissionInsurance,
                sharedCommissionInversion: sharedCommissionInversion,
                sharedCommissionMaintenance: sharedCommissionMaintenance,
                sharedCommissionSmallBusiness: sharedCommissionSmallBusiness,
                sharedCommissionEcommerce: sharedCommissionEcommerce,
                sharedCommissionCosmetics: sharedCommissionCosmetics,
                monthNames: monthNames
            });
        });

    } else {
        res.redirect('/supervisor/login');
    }

});

app.post('/supervisor/dashboard/products-by-sector', function (req, res) {
    console.log(req.body);

    switch (req.body.sector) {

        case 'Telecomunicación & Energías':
            TelecommunicationModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;
        case  'Ahorro & Inversión':
            InvestmentModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;
        case 'Seguros  Generales':
            InsuranceModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;
        case 'Mantenimiento & Reparaciones':
            MaintenanceModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;
        case 'Autónomos & Empresa':
            SmallBusinessModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;
        case 'Alimentación & E-commerce':
            EcommerceModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;
        case 'Hogar & Bienestar':
            CosmeticsModel.find({}, 'product', function (err, products) {

                if (!err) {
                    res.json({products: products});
                    res.end();

                } else {
                    res.json({error: 'No se pudó obtener el listado de productos'});
                    res.end();

                }
            });
            break;


    }


});


/**
 * ===============================================================
 *  SUPERVISORS DASHBOARD (List of clients)
 * ===============================================================
 */



app.get('/supervisor/clients/index', function (req, res, next) {

    User.find({created_by: req.session.user._id, $where: "this.client == true"}, function (err, clients) {

        if (!err) {

            res.render('./supervisor/dashboard/clients/index', {
                heading: 'Listado de clientes',
                clients: clients

            });

        } else {
            return next(err);
        }
    });

});

app.get('/supervisor/clients/new', function (req, res) {

    res.render('./supervisor/dashboard/clients/new', {
        heading: 'Alta nuevo cliente'
    });
});

app.post('/supervisor/clients/new', function (req, res) {
    var item = req.body;

    //Check if the DNI/NIE  exists
    UserModel.findOne({identification_number: item.identification_number}, function (err, user) {
        // console.log( req.session.user._id);

        if (user && user.email === trim(item.email)) { //Check if the email  exists
            return res.json({error: 'Este email ya esta registrado a otro usuario'});


        } else if (user && user.identification_number === trim(item.identification_number)) {
            return res.json({error: 'Este DNI/DIE ya esta registrado a otro usuario'});


        } else {
            //  console.log( moment(item.date_of_birth,'DD/MM/YYYY').utc().format());
            if (req.session && req.session.user) {
                var client = new UserModel({
                    first_name: item.first_name,
                    last_name: item.last_name,
                    identification_number: item.identification_number,
                    email: item.email,
                    telephone: item.telephone,
                    mobile: item.mobile,
                    address: item.address,
                    city: item.city,
                    post_code: item.post_code,
                    date_of_birth: moment(item.date_of_birth, 'DD/MM/YYYY').utc().format(),
                    client: true,
                    sex: item.sex,
                    created_by: req.session.user._id

                });

            } else {
                return res.json({info: 'login'});
            }

            client.save(function (err) {
                if (!err) {
                    return res.json({message: 'OK'});
                } else {
                    return res.json({error: 'Error, no se creó el nuevo cliente. Intentelo de nuevo más tarde'});
                }
            });
        }

    });

});

app.get('/supervisors/clients/edit/:id', function (req, res, next) {
    User.findOne({_id: req.params.id}, function (err, client) {
        if (!err) {
            res.render('./supervisor/dashboard/clients/edit', {
                heading: 'Editar cliente',
                client: client
            });
        } else {
            return next(err);
        }
    });

});

app.put('/supervisors/clients/edit/:id', function (req, res) {
    var item = req.body;

    UserModel.update({_id: req.params.id}, {
        $set: {
            first_name: item.first_name,
            last_name: item.last_name,
            identification_number: item.identification_number,
            email: item.email,
            telephone: item.telephone,
            mobile: item.mobile,
            address: item.address,
            city: item.city,
            post_code: item.post_code,
            date_of_birth: moment(item.date_of_birth, 'DD/MM/YYYY').utc().format(),
            client: true,
            sex: item.sex

        }
    }, function (err, info) {

        if (!err) {
            return res.json({message: 'OK'});
        } else {
            //console.log(err);
            return res.json({error: 'Error, vuelva a intentarlo más tarde'});
        }

    });


});

app.get('/supervisors/clients/show/:id', function (req, res, next) {

    User.findOne({_id: req.params.id}, function (err, client) {

        if (!err) {

            User.find({$where: "this.supervisor == true || this.admin == true"}, function (err, supervisors) {
                //console.log(clients);
                if (!err) {


                    //get client products
                    Product.find({client_info: client._id}).sort({created: -1}).exec(function (err, clientProducts) {


                        res.render('./supervisor/dashboard/clients/show', {
                            heading: 'Información cliente',
                            client: client,
                            supervisors: supervisors,
                            clientProducts: clientProducts
                        });

                    });



                } else {
                    res.json({error: {}});
                    res.end();
                    return next(err);
                }
            });


        } else {
            return next(err);
        }
    });

});


/**
 * ===============================================================
 *  SUPERVISORS BUSINESS CLIENTS (Empresas que son clientes)
 * ===============================================================
 */



app.get('/supervisor/business-clients/index', function (req, res, next) {

    User.find({created_by: req.session.user._id, $where: "this.business_client == true "}, function (err, clients) {
        //console.log(clients);
        if (!err) {

            res.render('./supervisor/dashboard/business_clients/index', {
                heading: 'Listado de  clientes',
                clients: clients

            });

        } else {
            return next(err);
        }
    });


});

app.get('/supervisor/business-clients/new', function (req, res) {

    res.render('./supervisor/dashboard/business_clients/new', {
        heading: 'Alta nuevo empresa cliente',
        clientId: req.query.client_id
    });
});

app.post('/supervisor/business-clients/new', function (req, res) {
    var item = req.body;

    //Check if the DNI/NIE  exists
    UserModel.findOne({identification_number: item.identification_number}, function (err, user) {
        // console.log( req.session.user._id);

        if (user && user.email === trim(item.email)) { //Check if the email  exists
            return res.json({error: 'Este email ya esta registrado a otro cliente'});


        } else if (user && user.identification_number === trim(item.identification_number)) {
            return res.json({error: 'Este CIF/NIF ya esta registrado a otro cliente'});
        } else {
            if (req.session && req.session.user) {
                var client = new UserModel({
                    first_name: item.first_name,
                    contact_person: item.contact_person,
                    identification_number: item.identification_number,
                    email: item.email,
                    telephone: item.telephone,
                    mobile: item.mobile,
                    fax: item.fax,
                    address: item.address,
                    city: item.city,
                    post_code: item.post_code,
                    province: item.province,
                    /*client: true,*/
                    business_client: true,
                    created_by: req.session.user._id

                });

            } else {
                res.json({message: 'login'});
            }

            client.save(function (err) {
                if (!err) {
                    return res.json({message: 'OK'});

                } else {
                    return res.json({error: 'Error, no se creó el nuevo cliente. Intentelo de nuevo más tarde'});

                }
            });
        }

    });

});

app.get('/supervisor/business-clients/show/:id', function (req, res, next) {

    if (req.session && req.session.user) {

        User.findOne({_id: req.params.id}, function (err, client) {

            if (!err) {

                //User.find({$where:"this.supervisor == true || this.admin == true"},function(err, supervisors){
                User.find({}, function (err, supervisors) {

                    if (!err) {


                        //get client products
                        Product.find({client_info: client._id}).sort({created: -1}).exec(function (err, clientProducts) {
                            //console.log(clientProducts );

                            res.render('./supervisor/dashboard/business_clients/show', {
                                heading: 'Información empresa cliente',
                                client: client,
                                supervisors: supervisors,
                                clientProducts: clientProducts
                            });

                        });




                    } else {

                        return next(err);
                    }
                });


            } else {
                return next(err);
            }
        });

    } else {
        res.redirect(301, '/login');
    }


});

app.get('/supervisor/business-clients/edit/:id', function (req, res, next) {
    User.findOne({_id: req.params.id}, function (err, client) {
        if (!err) {
            res.render('./supervisor/dashboard/business_clients/edit', {
                heading: 'Editar empresa cliente',
                client: client
            });
        } else {
            return next(err);
        }
    });

});

app.put('/supervisor/business-clients/edit/:id', function (req, res) {
    var item = req.body;

    //Check if the CIF/NIF  exists

    UserModel.update({_id: req.params.id}, {
        $set: {
            first_name: item.first_name,
            contact_person: item.contact_person,
            identification_number: item.identification_number,
            email: item.email,
            telephone: item.telephone,
            mobile: item.mobile,
            fax: item.fax,
            address: item.address,
            city: item.city,
            post_code: item.post_code,
            province: item.province

        }
    }, function (err, info) {

        if (!err) {
            return res.json({message: 'OK'});
        } else {

            return res.json({error: 'Error, vuelva a intentarlo más tarde o consulta el tecnico responsable'});
        }

    });

});


/**
 * =================================================================
 */


/**
 * ===============================================================
 *  SUPERVISORS DASHBOARD (Products)
 * ===============================================================
 */

app.get('/supervisors/products/new/', function (req, res) {
    res.render('./supervisor/dashboard/products/new', {clientId: req.query.client_id});
});

app.post('/supervisors/products/new', function (req, res) {

    var newFileName = randomString({
        length: 12,
        numeric: true,
        letters: false,
        special: false
    });
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {


        if (req.session && req.session.user) {
            if (!err) {


                if (!files.customer_info_doc) {
                    var toSave = {
                        sector: fields.product_sector,
                        name: fields.product_name,
                        observations: fields.observations,
                        contract_state: fields.contract_state,
                        client_info: fields.client_id,
                        supervisor_info: req.session.user._id,
                    };
                    var productItem = new Product(toSave);
                    productItem.save(function (err) {
                        if (!err) {
                            productItem = null;
                            return res.json({message: 'OK'});

                        } else {
                            //console.log(err);
                            productItem = null;
                            return res.json({error: 'Error, no se creó el nuevo producto. Intentelo de nuevo más tarde'});
                        }
                    });

                } else {
                    cloudinary.uploader.upload(files.customer_info_doc.path, function (result) {


                        var toSave = {
                            sector: fields.product_sector,
                            name: fields.product_name,
                            observations: fields.observations,
                            contract_state: fields.contract_state,
                            document_1: result,
                            client_info: fields.client_id,
                            supervisor_info: req.session.user._id,

                        };

                        var productItem = new Product(toSave);
                        productItem.save(function (err) {
                            if (!err) {
                                productItem = null;
                                return res.json({message: 'OK'});

                            } else {
                                //console.log(err);
                                productItem = null;
                                return res.json({error: 'Error, no se creó el nuevo producto. Intentelo de nuevo más tarde'});
                            }
                        });

                    }, {public_id: 'signed_contracts/' + newFileName, resource_types: 'raw'});
                }


            } else {

                res.json({error: 'Error procesando el formulario'})
            }
        } else {
            return res.json({info: 'login'});
        }


    });


});

app.get('/supervisors/products/edit/:id', function (req, res, next) {

    Product.find({_id: req.params.id}, function (err, editProduct) {
        if (!err) {
            User.find({$where: "this.supervisor == true || this.admin == true"}, function (err, supervisors) {

                if (!req.query) {
                    res.render('./supervisor/dashboard/products/edit',
                        {
                            heading: 'Editar producto',
                            editProduct: editProduct,
                            supervisors: supervisors
                        });
                } else {
                    res.render('./supervisor/dashboard/products/edit',
                        {
                            heading: 'Editar producto',
                            editProduct: editProduct,
                            supervisors: supervisors,
                            redirectMessage: 'Suba los documentos necesarios'
                        });
                }

            });

        } else {
            return next(err);
        }
    })


});

app.put('/supervisors/products/edit/:id', function (req, res) {



    if (req.session && req.session.user) {
        var item = req.body;
        Product.update({_id: req.params.id}, {
            $set: {
                sector: req.body.product_sector,
                name: req.body.product_name,
                observations: req.body.observations,
                contract_state: req.body.contract_state
            }
        }, function (err, product) {

            if (!err) {
                return res.json({message: 'OK'});

            } else {
                return res.json({error: 'Error, no se modificó  el producto. Intentelo de nuevo más tarde'});

            }
        });


    } else {
        res.json({info: 'login'});
    }


});

/**
 * SUPERVISORS UPLOAD DOCS FOR CLIENTS PRODUCTS
 * ==============================================================
 */

app.post('/supervisor/clients-doc-upload-1', function (req, res) {

    var newFileName = randomString({
        length: 12,
        numeric: true,
        letters: false,
        special: false
    });

    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
        if (!err) {
            cloudinary.uploader.upload(files.uploadedFile.path, function (results) {

                Product.update({_id: fields.product_id},
                    {$set: {document_1: results, document_name_1: fields.filename1}},
                    function (err) {
                        if (!err) {
                            res.json(results);
                            res.end();
                        } else {
                            res.json({error: 'Error, no se guardó bien'});
                            // console.log(err);
                            res.end();
                        }

                    });

            }, {public_id: 'signed_contracts/' + newFileName, resource_types: 'raw'});
        } else {
            res.json({error: 'Error procesando el formulario'});
        }


    });
});


app.post('/supervisor/clients-doc-upload-2', function (req, res) {


    var newFileName = randomString({
        length: 12,
        numeric: true,
        letters: false,
        special: false
    });


    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
        if (!err) {
            cloudinary.uploader.upload(files.uploadedFile.path, function (results) {

                Product.update({_id: fields.product_id},
                    {$set: {document_2: results, document_name_2: fields.filename2}},
                    function (err) {
                        if (!err) {
                            res.json(results);
                            res.end();
                        } else {
                            res.json({error: 'Error, no se guardó el documento'});
                            //console.log(err);
                            res.end();
                        }

                    });

            }, {public_id: 'signed_contracts/' + newFileName, resource_types: 'raw'});

        } else {
            res.json({error: 'Error processando el formulario'});
        }

    });


});

app.post('/supervisor/clients-doc-upload-3', function (req, res) {


    var newFileName = randomString({
        length: 12,
        numeric: true,
        letters: false,
        special: false
    });


    //res.end();
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {

        if (!err) {

            cloudinary.uploader.upload(files.uploadedFile.path, function (results) {

                Product.update({_id: fields.product_id},
                    {$set: {document_3: results, document_name_3: fields.filename3}},
                    function (err) {
                        if (!err) {
                            res.json(results);
                            res.end();
                        } else {
                            res.json({error: 'Error, no se guardó el documento'});
                            //console.log(err);
                            res.end();
                        }

                    });

            }, {public_id: 'signed_contracts/' + newFileName, resource_types: 'raw'});

        } else {
            res.json({error: 'Error procesando forulario'});
        }

    });


});

/**
 * ===============================================================
 *  SUPERVISORS DASHBOARD (Documents for download)
 * ===============================================================
 */

app.get('/supervisor/documents/index', function (req, res) {
    res.render('./supervisor/dashboard/documents/index', {
        heading: 'Documentación de solicitud'
    });
});


app.get('/supervisor/documents/supervisor', function (req, res) {
    res.render('./supervisor/dashboard/documents/supervisor', {
        heading: 'Documentación  para la gestión de asesores'
    });
});


app.get('/supervisor/documents/general', function (req, res) {
    res.render('./supervisor/dashboard/documents/general', {
        heading: 'Documentación '
    });
});


app.get('/supervisor/documents/training', function (req, res) {
    res.render('./supervisor/dashboard/documents/training', {
        heading: 'Documentación de formación'
    });
});


app.get('/supervisor/documents/commission', function (req, res) {
    res.render('./supervisor/dashboard/documents/commission', {
        heading: 'Documentación sobre comisiones'
    });
});


app.get('/supervisor/documents/auga', function (req, res) {
    res.render('./supervisor/dashboard/documents/auga', {
        heading: 'Documentación auga'
    });
});


app.get('/supervisor/documents/small_business', function (req, res) {
    res.render('./supervisor/dashboard/documents/small_business', {
        heading: 'Documentación Autónomos y empresa'
    });
});

/**
 * ===============================================================
 *  CONTACT US FORM
 * ===============================================================
 */


/**
 * ===============================================================
 *  SUPERVISORS DASHBOARD (Create client products incidents or observatios)
 * ===============================================================
 */


app.get('/supervisor/product-notification/new/', function (req, res) {

    res.render('./supervisor/dashboard/product_notifications/new', {
        productId: req.query.product_id,
        productState: req.query.product_state
    });
});

app.post('/supervisor/product-notification/new/', function (req, res) {


    var newFileName = randomString({
        length: 12,
        numeric: true,
        letters: false,
        special: false
    });
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
        //console.log(fields);
        //console.log(files);
        if (!err) {
            if (req.session && req.session.user) {

                if (files && files.uploadedFile.name !== '') {
                    //console.log(files.uploadedFile.path);

                    cloudinary.uploader.upload(files.uploadedFile.path, function (result) {

                        Product.findOne({_id: fields.product_id}, function (err, product) {

                            if (!err) {
                                product.product_notifications.push({
                                    state: fields.product_state,
                                    created_by: req.session.user._id,
                                    remarks: fields.observations,
                                    attachment_name: fields.fileName1 || false,
                                    attachment: result,
                                    supervisor_seen: true // To avoid the supervisor seeing his own messages as not read
                                });


                                product.save(function (err) {
                                    if (!err) {
                                        res.json({message: 'OK'});
                                    } else {
                                        res.json({error: 'Error, no se creó la nueva notificación'});
                                        //res.end();
                                    }
                                });
                            } else {
                                res.json({error: 'Error recuperando el producto con id' + fields.product_id});
                                //res.end();
                            }
                        })

                    }, {public_id: 'product_observations_attachments/' + newFileName, resource_types: 'raw'});

                } else {

                    //var parent = new Product({ product_notifications: [{ state:fields.product_state,}] })
                    Product.findOne({_id: fields.product_id}, function (err, product) {

                        if (!err) {
                            product.product_notifications.push({
                                state: fields.product_state,
                                created_by: req.session.user._id,
                                remarks: fields.observations,
                                supervisor_seen: true // To avoid the supervisor seeing his own messages as not read
                            });


                            product.save(function (err) {
                                if (!err) {
                                    res.json({message: 'OK'});
                                } else {
                                    res.json({error: 'Error, no se creó la nueva notificación'});

                                }
                            });
                        } else {
                            res.json({error: 'Error recuperando el producto con id' + fields.product_id});
                            //res.end();
                        }
                    })

                }


            } else {
                res.json({info: 'login'});
                res.end();
            }
        } else {
            es.json({error: 'Error procesando el formulario'});
        }


    });
});

app.get('/supervisor/product-notification/show/:id', function (req, res, next) {

    Product.findOne()
        .where('_id').equals(req.params.id)
        .populate('product_notifications.created_by', 'first_name last_name')
        .exec(function (err, notifications) {

            //Set superviosr_read filed to true
            _.forEach(notifications.product_notifications, function (item) {

                if (item.supervisor_seen == false) {
                    item.supervisor_seen = true;
                    notifications.save(function (err) {
                        if (err) {
                            return next(err);
                        } else {
                            //console.log('All ok');
                        }
                    });
                }


            });


            notifications.product_notifications.reverse();
            if (!err) {
                res.render('./supervisor/dashboard/product_notifications/show',
                    {notifications: notifications.product_notifications});

            } else {
                return next(err);
            }
        });

});


/**
 * ===============================================================
 *  SUPERVISORS DASHBOARD (Create new supervisor account and recommend them to the admin for account activation)
 * ===============================================================
 */

app.get('/supervisor/recommend-supervisor', function (req, res, next) {
    if (req.session && req.session.user) {
        UserModel.find()
            .where('supervisor_recommendation_history.created_by').equals(req.session.user._id)
            .exec(function (err, supervisors) {

                if (!err) {
                    res.render('./supervisor/dashboard/supervisor_recommendation/index',
                        {supervisors: supervisors});
                } else {

                    return next(err);
                }

            });


    } else {
        res.redirect(301, '/supervisor/login');
    }
});

app.get('/supervisor/recommend-supervisor/new', function (req, res) {
    //console.log(req.query.product_id);
    res.render('./supervisor/dashboard/supervisor_recommendation/new');
});

app.post('/supervisor/recommend-supervisor/new', function (req, res) {


    if (req.session && req.session.user) {


        var newFileName = randomString({
            length: 12,
            numeric: true,
            letters: false,
            special: false
        });

        var form = new formidable.IncomingForm();

        form.parse(req, function (err, fields, files) {
            if (!err) {
                //Check if the email exists
                UserModel.count({email: fields.email}, function (err, count) {

                    if (count >= 1) {
                        res.json({error: 'Este email ya esta registrado a otro usuario'});
                        res.end();
                        return
                    }

                });


                //Check if the DNI/NIE  exists
                UserModel.count({identification_number: fields.identification_number}, function (err, count) {

                    if (count >= 1) {
                        res.json({error: 'Este DNI/DIE ya esta registrado a otro usuario'});
                        res.end();
                        return
                    } else {


                        cloudinary.uploader.upload(files.supervisor_info.path, function (result) {
                            var doc = result;
                            var Sup = new UserModel({
                                first_name: fields.first_name,
                                last_name: fields.last_name,
                                identification_number: fields.identification_number,
                                email: fields.email,
                                telephone: fields.telephone,
                                mobile: fields.mobile,
                                address: fields.address,
                                city: fields.city,
                                post_code: fields.post_code,
                                date_of_birth: moment(fields.date_of_birth, 'DD/MM/YYYY').utc().format(),
                                supervisor: true,
                                supervisor_recommendation_document: result,
                                supervisor_recommendation_history: [{
                                    state: 'recomendado',
                                    created_by: req.session.user._id
                                }]
                            });


                            Sup.save(function (err) {
                                if (!err) {
                                    res.json({message: 'Supervisor creado corectamente.'});
                                    //res.end();
                                } else {
                                    res.json({error: 'Error, no se creó el nuevo acesor. Inténtelo de nuevo más tarde'});
                                    //res.end();
                                    console.log(err);
                                }


                            });

                        }, {public_id: 'user_info/' + newFileName, resource_types: 'raw'});


                    }

                });
            } else {
                res.json({error: 'Error procesando formulario'});
                res.end();
            }

        });


    } else {
        res.redirect('/login');
    }


});


app.get('/supervisor/supervisors/show/:id', function (req, res, next) {
    UserModel.findOne({_id: req.params.id}, function (err, supervisor) {
        if (!err) {

            res.render('./supervisor/dashboard/supervisor_recommendation/show',
                {
                    heading: 'Datos asesor',
                    supervisor: supervisor,
                    history: supervisor.supervisor_recommendation_history
                });
        } else {
            return next(err);
        }

    });


});


app.get('/supervisor/recommend-supervisor/edit/:id', function (req, res, next) {

    UserModel.findOne({_id: req.params.id}, function (err, supervisor) {

        //console.log(supervisor);
        if (!err) {

            res.render('./supervisor/dashboard/supervisor_recommendation/edit',
                {
                    heading: 'Modificar datos  asesor',
                    supervisor: supervisor,
                    history: supervisor.supervisor_recommendation_history
                });
        } else {
            return next(err);
        }

    });


});


app.put('/supervisor/recommend-supervisor/edit/:id', function (req, res) {
    var sup = req.body;

    UserModel.update({_id: req.params.id},
        {
            $set: {
                first_name: sup.first_name,
                last_name: sup.last_name,
                identification_number: sup.identification_number,
                email: sup.email,
                telephone: sup.telephone,
                mobile: sup.mobile,
                address: sup.address,
                city: sup.city,
                post_code: sup.post_code,
                date_of_birth: moment(sup.date_of_birth, 'DD/MM/YYYY').utc().format()
                /*account_creation_state: {state: sup.account_creation_state, updated: new Date().toISOString()}*/
            }
        }, function (err, supervisor) {
            if (!err) {
                //console.log(supervisor);
                res.json({message: 'Asssor modificado corectamente'});
                res.end();
            } else {
                res.json({error: 'Error, no se guardó los cambios, intentelo de nuevo maś tarde'});
                res.end();
            }

        });
});

/**
 * ===============================================================
 *  SUPERVISORS COMMISSIONS
 * ===============================================================
 */

app.get('/supervisor/commissions/index', function (req, res, next) {

    var currentMonth = parseInt(moment(new Date()).format('MM'));
    var quarter;
    var check = {};


    async.parallel({
        products: function (callback) {
            Product.find()
                .where('supervisor_info').equals(req.session.user._id)
                .where('payment_received_date').ne(null)
                .where('personal_commission_payment').equals(false)
                .where('contract_state').equals('Comisionable')
                .populate('client_info')
                .exec(function (err, products) {
                    if (!err) {
                        //console.log(products.length);
                        callback(err, products);
                    }
                });



        },
        personalCommission: function (callback) {
            Product.aggregate([
                {
                    $match: {
                        supervisor_info: new objectId(req.session.user._id)
                    }
                },
                {
                    $match: {
                        payment_received_date: {$ne: null}
                    }
                },
                {
                    $match: {
                        contract_state: 'Comisionable'
                    }
                },
                {
                    $match: {
                        personal_commission_payment: {$eq: false}
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalPersonalCommission: {$sum: "$personal_commission"}
                    }
                }


            ], function (err, data) {
                console.log(data);
                callback(err, data);

            });

        },
        proCommission: function (callback) {
            Product.aggregate([
                {
                    $match: {
                        supervisor_info: new objectId(req.session.user._id)
                    }
                },
                {
                    $match: {
                        payment_received_date: {$ne: null}
                    }
                },
                {
                    $match: {
                        personal_commission_payment: {$eq: false}
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalProCommission: {$sum: "$pro_supervisor_commission"}
                    }
                }

            ], function (err, data) {

                callback(err, data);

            });

        },
        paymentsPending: function (callback) {
            if (req.session && req.session.user) {

                PersonalCommissionNotifications.count()
                    .where('requested_by').equals(req.session.user._id)
                    .where('paid').equals(false)
                    .exec(function (err, pending) {
                        callback(err, pending);
                    });
            } else {
                res.redirect(301, '/supervisor/login');
            }

        }
    }, function (err, results) {
        if (!err) {
            var productCount = (results.products.length > 0 ) ? results.products : 0;
            var paymentsPending = results.paymentsPending;
            var proCommission = (results.proCommission.length > 0) ? results.proCommission[0].totalProCommission : 0;
            res.render('./supervisor/dashboard/commissions/index', {
                products: results.products,
                personalCommission: (results.personalCommission.length > 0) ? results.personalCommission[0].totalPersonalCommission : 0,
                paymentsPending: results.paymentsPending,
                productCount: productCount,
                totalProCommission: proCommission
            });
        } else {
            next(err);
        }
    });


});

app.get('/supervisor/commissions/show', function (req, res) {
    res.render('./supervisor/dashboard/commissions/show');
});

app.get('/supervisor/commissions/recieved', function (req, res, next) {

    Product.find()
        .where('supervisor_info').equals(req.session.user._id)
        .where('personal_commission_payment.date').ne(null)
        .populate('client_info')
        .sort({created: -1})
        .exec(function (err, comisionedProducts) {
            //console.log(comisionedProducts);
            if (!err) {
                res.render('./supervisor/dashboard/commissions/recieved',
                    {comisionedProducts: comisionedProducts});
            } else {
                return next(err);
            }

        });


});

app.get('/supervisor/commissions/contracted', function (req, res, next) {

    var currentMonth = parseInt(moment(new Date()).format('MM'));
    var quarter;
    var check = {};


    async.parallel({
        products: function (callback) {
            Product.find()
                .where('supervisor_info').equals(req.session.user._id)
                .where('payment_received_date').ne(null)
                .where('personal_commission_payment').equals(false)
                .where('contract_state').equals('Contratado')
                .populate('client_info')
                .exec(function (err, products) {
                    if (!err) {
                        console.log(products.length);
                        callback(err, products);
                    }
                });


        },
        personalCommission: function (callback) {
            Product.aggregate([
                {
                    $match: {
                        supervisor_info: new objectId(req.session.user._id)
                    }
                },
                {
                    $match: {
                        payment_received_date: {$ne: null}
                    }
                },
                {
                    $match: {
                        contract_state: 'Contratado'
                    }
                },
                {
                    $match: {
                        personal_commission_payment: {$eq: false}
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalPersonalCommission: {$sum: "$personal_commission"}
                    }
                }


            ], function (err, data) {
                console.log(data);
                callback(err, data);

            });

        },
        proCommission: function (callback) {
            Product.aggregate([
                {
                    $match: {
                        supervisor_info: new objectId(req.session.user._id)
                    }
                },
                {
                    $match: {
                        payment_received_date: {$ne: null}
                    }
                },
                {
                    $match: {
                        personal_commission_payment: {$eq: false}
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalProCommission: {$sum: "$pro_supervisor_commission"}
                    }
                }

            ], function (err, data) {

                callback(err, data);

            });

        },
        paymentsPending: function (callback) {
            if (req.session && req.session.user) {

                PersonalCommissionNotifications.count()
                    .where('requested_by').equals(req.session.user._id)
                    .where('paid').equals(false)
                    .exec(function (err, pending) {
                        callback(err, pending);
                    });
            } else {
                res.redirect(301, '/supervisor/login');
            }

        }
    }, function (err, results) {
        if (!err) {
            var productCount = (results.products.length > 0 ) ? results.products : 0;
            var paymentsPending = results.paymentsPending;
            var proCommission = (results.proCommission.length > 0) ? results.proCommission[0].totalProCommission : 0;
            res.render('./supervisor/dashboard/commissions/contracted', {
                products: results.products,
                personalCommission: (results.personalCommission.length > 0) ? results.personalCommission[0].totalPersonalCommission : 0,
                paymentsPending: results.paymentsPending,
                productCount: productCount,
                totalProCommission: proCommission
            });
        } else {
            next(err);
        }
    });


});

app.get('/supervisor/commissions/pro-commission', function (req, res) {
    if (req.session && req.session.user) { //check for session


        async.parallel({
            products: function (callback) {
                Product.find()
                    .where('pro_supervisor_info').equals(req.session.user._id)
                    .where('payment_received_date').equals(null)
                    .where('personal_commission_payment').equals(false)
                    .where('contract_state').equals('Contratado')
                    .exec(function (err, products) {
                        callback(err, products);
                    });

            },
            proComission: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            pro_supervisor_info: new objectId(req.session.user._id)
                        }
                    },
                    {
                        $match: {
                            payment_received_date: {$ne: null}
                        }
                    },
                    {
                        $match: {
                            contract_state: 'Contratado'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            totalProCommission: {$sum: "$pro_supervisor_commission"}
                        }
                    }
                ], function (err, data) {
                    //console.log(data);
                    callback(err, data);
                });
            },
        }, function (err, results) {
            //console.log(results.products);
            //console.log(results.proComission[0].totalProCommission);
            var proCommission = (results.proComission.length > 0 ) ? results.proComission[0].totalProCommission : 0;
            res.render('./supervisor/dashboard/commissions/pro_commission',
                {
                    products: results.products,
                    proCommission: proCommission
                });
        });


        //TODO sum pro supervisor commision


    } else {
        res.render('./supervisor/login');
    }
});


app.get('/supervisor/commissions/recieved-pro-commission', function (req, res, next) {
    if (req.session && req.session.user) { //check for session
        Product.find()
            .where('pro_supervisor_info').equals(req.session.user._id)
            .where('payment_received_date').ne(null)
            /*.where('personal_commission_payment').equals(false) */
            .where('contract_state').equals('Contratado')
            .populate('client_info')
            .sort({created: -1})
            .exec(function (err, products) {
                if (!err) {
                    res.render('./supervisor/dashboard/commissions/recieved_pro_commission',
                        {
                            products: products,
                        });
                } else {
                    return next(err);
                }

            });
    } else {
        res.render('./supervisor/login');
    }

});

/**
 * ===============================================================
 *  SUPERVISORS PUNCTUATIONS
 * ===============================================================
 */

app.get('/supervisor/punctuation/index', function (req, res, next) {


    async.auto({
        currentSupervisor: function (callback) {
            UserModel.findOne()
                .where('_id').equals(req.session.user._id)
                .select('first_name last_name')
                .exec(function (err, user) {
                    callback(err, user);
                    //console.log(user);
                });
        },
        supervisorsList: function (callback) {

            UserModel.find()
                .where('supervisor_recommendation_history.created_by').equals(req.session.user._id)
                .select('first_name last_name')
                .exec(function (err, users) {
                    //console.log(users);
                    callback(err, users);
                });


        },
        punctuations: ['supervisorsList', function (callback, res) {
            var supervisorIds = [];
            for (i = 0; i < res.supervisorsList.length; i++) {
                supervisorIds.push(res.supervisorsList[i]._id);
            }

            //console.log(supervisorIds);
            //console.log(res.supervisorsList);
            callback();

            Product.aggregate([
                {
                    $match: {
                        supervisor_info: {"$in": supervisorIds}
                    }
                },


                {
                    $group: {
                        _id: "$supervisor_info",
                        sector: {$push: "$sector"},
                    }
                }


            ], function (err, data) {
                //console.log(JSON.stringify(data));
                callback(err, data);
            })


        }]


    }, function (err, results) {
        if (!err) {


            res.render('./supervisor/dashboard/punctuation/index', {
                result: results,
                res: JSON.stringify(results),
            });

        } else {
            next(err);
        }
    });



});

app.post('/supervisor/punctuation/sub-supervisor-sales-info', function (req, res, next) {


    async.parallel({
            cosmetics: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },
                    {
                        $match: {
                            sector: "Hogar & Bienestar"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);
                        //return results;
                    } else {
                        return next(err);
                    }
                })

            },
            telecom: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },

                    {
                        $match: {
                            sector: "Telecomunicación & Energías"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {

                        callback(err, results);

                    } else {
                        return next(err);
                    }
                })

            },

            insurance: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },

                    {
                        $match: {
                            sector: "Seguros  Generales"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {

                        callback(err, results);

                    } else {
                        return next(err);
                    }
                })

            },

            investment: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },

                    {
                        $match: {
                            sector: "Ahorro & Inversión"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {

                        callback(err, results);

                    } else {
                        return next(err);
                    }
                })

            },

            maintenance: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },

                    {
                        $match: {
                            sector: "Mantenimiento & Reparaciones"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);

                    } else {
                        return next(err);
                    }
                })

            },

            small_business: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },

                    {
                        $match: {
                            sector: "Autónomos & Empresa"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);

                    } else {
                        return next(err);
                    }
                })

            },

            ecommerce: function (callback) {
                Product.aggregate([
                    {
                        $match: {
                            supervisor_info: new mongoose.Types.ObjectId(req.body.subSupervisorId)
                        }
                    },

                    {
                        $match: {
                            sector: "Alimentación & E-commerce"
                        }
                    },
                    {
                        $project: {
                            sector: 1,
                            points: 1,
                            month: {$month: '$created'}
                        }
                    },
                    {$match: {month: (parseInt(new Date().getMonth()) + 1)}},
                    {
                        $group: {
                            _id: '$sector',
                            sales: {$sum: '$points'}
                        }
                    },


                ], function (err, results) {
                    if (!err) {
                        // console.log(results.length);
                        callback(err, results);

                    } else {
                        return next(err);
                    }
                })

            },


        },
        function (err, finalResults) {
            // results
            var cosmetics = (finalResults.cosmetics.length > 0) ? finalResults.cosmetics[0].sales : 0.00;
            var telecom = (finalResults.telecom.length > 0) ? finalResults.telecom[0].sales : 0.00;
            var insurance = (finalResults.insurance.length > 0) ? finalResults.cosmetics[0].sales : 0.00;
            var investment = (finalResults.investment.length > 0) ? finalResults.investment[0].sales : 0.00;
            var small_business = (finalResults.small_business.length > 0) ? finalResults.small_business[0].sales : 0.00;
            var maintenance = (finalResults.maintenance.length > 0) ? finalResults.maintenance[0].sales : 0.00;
            var ecommerce = (finalResults.ecommerce.length > 0) ? finalResults.ecommerce[0].sales : 0.00;
            //console.log(finalResults);
            if (err) {
                next(err);
            }
            res.json({
                message: 'OK',
                cosmetics: cosmetics,
                telecom: telecom,
                insurance: insurance,
                investment: investment,
                small_business: small_business,
                maintenance: maintenance,
                ecommerce: ecommerce
            });

        });


});


app.post('/supervisor/commissions/request-payment', function (req, res) {


    var commissionRequest = new PersonalCommissionNotifications({
        requested_by: req.body.supervisorId
    });

    commissionRequest.save(function (err) {
        if (!err) {
            return res.json({message: 'OK'});

        } else {
            return res.json({error: 'error'});

        }
    });


});

/**
 * ===============================================================
 *  SUPERVISORS PASSWORD
 * ===============================================================
 */

app.get('/change-password/:hash', function (req, res) {

    PasswordChange.findOne({hash: req.params.hash}, function (err, data) {
        //console.log(data);
        if (data) {
            res.render('./password_change/new', {userid: data.user_id});
        } else {
            res.send('<h1> Este enlace para cambair la contraseña ha caducado!</h1>');
            res.end();
        }
    });

});

app.post('/change-password/', function (req, res, next) {


    bcrypt.genSalt(10, function (err, salt) {
        if (err) return next(err);

        bcrypt.hash(req.body.password, salt, null, function (err, hash) {
            if (err) return next(err);


            UserModel.update({_id: req.body.userid}, {$set: {password: hash}}, function (err) {
                if (!err) {
                    res.json({message: 'OK'});
                    res.end();

                    //Delete password hash from database
                    PasswordChange.remove({user_id: req.body.userid}, function (err) {
                        if (err) {
                            return next(err);
                        }
                    });

                } else {
                    res.json({error: 'Error, no se actualizó la contraseña, inténtelo de nuevo más tarde'});

                }
            });

        })
    });


});


/**
 * ===============================================================
 *  SUPERVISORS RECOVER PASSWORD
 * ===============================================================
 */

app.get('/supervisor/recover-password', function (req, res) {
    res.render('./supervisor/recover_password');
});


app.post('/supervisor/recover-password', function (req, res) {

    //check if email exists
    UserModel.findOne({email: trim(req.body.email)}, function (err, user) {
        if (!err && (user !== null)) {

            var emailData = {};
            emailData.templatePath = './templates/recover_password_supervisor.hbs';
            emailData.subject = 'Cambio de contraseña - Unosiete.us';

            emailData.to = req.body.email;
            emailData.from = 'administracion@unosiete.us';
            emailData.fromname = 'Administracion Unosiete';
            emailData.username = user.first_name + ' ' + user.last_name;
            emailData.hash = uuid.v1();
            //emailData.domain                  =   req.get('referer') || req.get('host');
            emailData.domain = req.get('host');
            emailData.protocol = req.protocol;

            if (emailData.hash) {
                var changePass = new PasswordChange({hash: emailData.hash, email: trim(req.body.email)});
                changePass.save(function (err) {

                    if (!err) {
                        res.json({message: 'OK'});

                        sendMail(emailData);

                    } else {
                        res.json({error: 'Error en la activación de la cuenta del asesor'});

                    }

                });
            } else {
                res.json({error: 'Error en la creación del código de activacion'});

            }


        } else {
            res.json({error: 'Este emial no ha sido encontrado, comprueba e intentelo de nuevo'});

        }

    });


});

app.get('/supervisor/recover-password/new-password/:hash', function (req, res) {
    //console.log(req.params.hash);

    res.render('./supervisor/new_password', {hash: req.params.hash});
});


app.post('/supervisor/recover-password/new-password', function (req, res, next) {

    console.log(req.body);


    PasswordChange.findOne({hash: req.body.hash}, function (err, data) {
        if (!err) {
            //res.json({da:data});
            bcrypt.genSalt(10, function (err, salt) {
                if (err) return next(err);

                bcrypt.hash(trim(req.body.password), salt, null, function (err, hashPass) {
                    if (err) return next(err);

                    console.log(hashPass);
                    UserModel.update({email: data.email}, {$set: {password: hashPass}}, function (err) {
                        if (!err) {
                            res.redirect(301, '/supervisor/login');
                            //Delete has from DB table
                            PasswordChange.find({hash: req.body.hash}).remove().exec();
                        } else {
                            console.log(err);
                        }
                    });

                })
            });
        } else {
            res.json({error: 'Error en la creacion de su nueva contraseña'});
        }

    });

});


/**
 * ===============================================================
 *  CONTACT US FORM
 * ===============================================================
 */

app.post('/contact-us', function (req, res) {

    //Send  email to unosiete office
    var emailData = {};
    emailData.templatePath = './templates/request_info.hbs';
    emailData.subject = req.body.subject;
    emailData.to = 'hola@unosiete.us';
    emailData.from = req.body.email;
    emailData.username = req.body.name;
    emailData.message = req.body.message;

    //var emailData = req.body;
    sendMail(emailData);
    //console.log(req.body);
    res.json({message: '<b>Su mensaje ha sido enviado!</b>'});
    res.end();


});


if (app.get('env') === 'production' || app.get('env') === 'staging' || process.env.APP_ENV === 'production' || process.env.APP_ENV === 'staging') {
    /**
     Error and exception handling
     */


    app.get('*', function (req, res, next) {
        var err = new Error();
        err.status = 404;
        next(err);
    });

    // handling 404 errors
    app.use(function (err, req, res, next) {
        if (err.status !== 404) {

            return next();
        }
        var message = err.message || '404 - No hemos encontrado lo que buscas';

        res.render('./error/404', {heading: message});


    });

    app.use(function (err, req, res, next) {
        // log the error, treat it like a 500 internal server error
        // maybe also log the request so you have more debug information
        //log.error(err, req);

        // during development you may want to print the errors to your console
        // console.log(err.stack);

        // send back a 500 with a generic message
        res.status(500);
        res.render('./error/500', {heading: 'Parece que se ha roto algo. Lo investigaremos para coregirlo '});
        //res.send('oops! something broke');
    });

    process.on('uncaughtException', function (err) {
        // handle the error safely
        //console.log(err);
        //TODO log all errors to LOGLY online service
        console.log('BAD ERROR HAS HAPPENED' + new Date());
    })

} else {


    app.get('*', function (req, res, next) {
        var err = new Error();
        //err.status = 404;
        //next(err);
        console.log(JSON.stringify(err));
    });

    // handling 404 errors
    app.use(function (err, req, res, next) {
        if (err.status !== 404) {
            //res.json({error: 'Not found'})
            return next();
        }
        var message = err.message || '<h1>404 - No hemos encontrado lo que buscas</h1>';
        console.log(message);
        console.log(err);
        //res.render('./error/404', {heading: message});
        //res.send(err.message || '** no unicorns here **');

    });


    app.use(function (err, req, res, next) {
        // log the error, treat it like a 500 internal server error
        // maybe also log the request so you have more debug information
        //log.error(err, req);

        // during development you may want to print the errors to your console
        // console.log(err.stack);

        // send back a 500 with a generic message
        res.status(500);
        console.log(err);
        console.log(err.stack);
        //res.render('./error/500', {heading: 'Ooooh no..... parece que se ha roto algo. Lo investigaremos para coregirlo '});

    });


    //Catch all uncaught errors
    process.on('uncaughtException', function (err) {
        // handle the error safely
        console.log(err);
        console.log('BAD ERROR HAS HAPPENED' + new Date());
    });


}


app.listen(port, function () {
    console.log('App listening on port : ' + port);
});
