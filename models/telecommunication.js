var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var TelecommunicationSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  Number,
    created: { type: Date, default: Date.now }
});

TelecommunicationSchema.index({product: 'text'}); //Indexing for fast db queries and text search
Telecommunication = mongoose.model('Telecommunication', TelecommunicationSchema);


module.exports = Telecommunication ;
