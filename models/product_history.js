var mongoose            = require('mongoose');

var Schema              = mongoose.Schema;

var ProductHistorySchema  = new Schema({
    product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    product_state:  {type: String, default: false},
    observation:   {type: String, default: false},
    document_name_1:  {type: String, default: false},
    document_name_2:  {type: String,default: false},
    document_name_3:  {type: String, default: false},
    document_1: {type:Schema.Types.Mixed, default: undefined},
    document_2: {type:Schema.Types.Mixed, default: undefined},
    document_3: {type:Schema.Types.Mixed, default: undefined},
    created: { type: Date, default: Date.now }
});


ProductHistory = mongoose.model('Product_History', ProductHistorySchema);

module.exports = ProductHistory ;
