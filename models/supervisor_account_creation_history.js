var mongoose            = require('mongoose');

var Schema              = mongoose.Schema;

var SupervisorRecommendationHistorySchema  = new Schema({
    state: {type:String},
    updated_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    updated: { type: Date, default: Date.now }
});


SupervisorRecommendationHistorySchema = mongoose.model('supervisor_recommendation_history', SupervisorRecommendationHistorySchema);

module.exports = SupervisorRecommendationHistorySchema ;
