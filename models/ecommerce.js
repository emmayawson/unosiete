var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var EcommerceSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  String,
    created: { type: Date, default: Date.now }
});

EcommerceSchema.index({product: 'text'}); //Indexing for fast db queries and text search
Ecommerce = mongoose.model('Ecommerce', EcommerceSchema);

module.exports = Ecommerce ;
