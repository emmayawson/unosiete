var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var InvestmentSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  String,
    created: { type: Date, default: Date.now }
});

InvestmentSchema.index({product: 'text'}); //Indexing for fast db queries and text search
Investment = mongoose.model('Investment', InvestmentSchema);

module.exports = Investment ;

