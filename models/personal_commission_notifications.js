var mongoose            = require('mongoose');

var Schema              = mongoose.Schema;


var PersonalCommissionNotificationsSchema  = new Schema({
    paid: {type:String, default: false},
    requested_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    paid_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    admin_seen: {type:Boolean , default: false },
    /* supervisor_seen: {type:Boolean, default: false },
    remarks: {type:String, default: ' '},
    attachment_name: {type:String, default: false},
    attachment: {type: Schema.Types.Mixed, default: false},*/
    date_requested: { type: Date, default: Date.now },
    date_paid: { type: Date}
});

PersonalCommissionNotifications = mongoose.model('personal_commission_notifications', PersonalCommissionNotificationsSchema);



module.exports = PersonalCommissionNotifications ;