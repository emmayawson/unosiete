var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var SupervisorSchema  = new Schema({
    first_name: String,
    last_name: String,
    identification_number:  String,
    email:  String,
    telephone:  String,
    mobile:  String,
    address:  String,
    city:  String,
    post_code:  String,
    date_of_birth:  String,
    account_creation_state:  Schema.Types.Mixed,
    created: { type: Date, default: Date.now }
});


Supervisor = mongoose.model('Supervisor', SupervisorSchema);

module.exports = Supervisor ;
