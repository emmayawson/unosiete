var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var MaintenanceSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  String,
    created: { type: Date, default: Date.now }
});

MaintenanceSchema.index({product: 'text'}); //Indexing for fast db queries and text search
Maintenance = mongoose.model('Maintenance', MaintenanceSchema);

module.exports = Maintenance ;
