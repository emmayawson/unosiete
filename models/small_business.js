var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var SmallBusinessSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  String,
    created: { type: Date, default: Date.now }
});

SmallBusinessSchema.index({product: 'text'}); //Indexing for fast db queries and text search
SmallBusiness = mongoose.model('Small_Business', SmallBusinessSchema);

module.exports = SmallBusiness ;
