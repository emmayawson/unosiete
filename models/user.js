var mongoose            = require('mongoose');
var bcrypt 			    = require('bcrypt-nodejs');


var Schema              = mongoose.Schema;


var SupervisorRecommendationHistorySchema  = new Schema({
    state: {type:String},
    created_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, /*recomended by*/
    approved_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    updated: { type: Date, default: Date.now }
});


SupervisorRecommendationHistorySchema = mongoose.model('supervisor_recommendation_history', SupervisorRecommendationHistorySchema);




var UserSchema  = new Schema({
    first_name: String,
    last_name: String,
    contact_person: {type: String, default: ''},
    email:  String,
    sex:{type:String, default:false},
    identification_number:  String,
    telephone:  String,
    mobile:  String,
    fax:  {type:String, default: false},
    address:  String,
    city:  String,
    post_code:  String,
    province:  String,
    date_of_birth:  String,
    supervisor_recommendation_history: [SupervisorRecommendationHistorySchema.schema],
    supervisor_recommendation_document: {type:Schema.Types.Mixed, default: undefined},
    /*account_creation_state:  Schema.Types.Mixed,*/
    password: {type:String, default: 'abc123'},
    admin: {type:Boolean, default: false},
    supervisor: {type:Boolean, default: false},
    client: {type:Boolean, default: false},
    business_client: {type:Boolean, default: false},
    authorized:{type:Boolean, default: false},
    created_by: {type:String, default: false}, //For clients  and Supervisor recomendation only
    created: { type: Date, default: Date.now }
});



UserSchema.pre('save', function(next){
    var user = this ;
    //Hash user password
    bcrypt.genSalt(10, function(err, salt){
        if(err) return next(err);

        bcrypt.hash(user.password, salt, null, function(err, hash){
            if(err) return next(err);

            user.password = hash;
            next();
        })
    });



});

User = mongoose.model('User', UserSchema);

module.exports = User ;

//NOTE: to prevent mongoose from adding an _id field to the schema, do the following
//var noId = new Schema({ name:String, {_id: false}})

