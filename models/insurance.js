var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var InsuranceSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  String,
    created: { type: Date, default: Date.now }
});

InsuranceSchema.index({product: 'text'}); //Indexing for fast db queries and text search
Insurance = mongoose.model('Insurance', InsuranceSchema);

module.exports = Insurance ;

