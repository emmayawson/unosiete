var mongoose            = require('mongoose');

var Schema              = mongoose.Schema;


var ProductNotificationsSchema  = new Schema({
    state: {type:String},
    created_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    admin_seen: {type:Boolean , default: false },
    supervisor_seen: {type:Boolean, default: false },
    remarks: {type:String, default: ' '},
    attachment_name: {type:String, default: false},
    attachment: {type: Schema.Types.Mixed, default: false},
    updated: { type: Date, default:Date.now}
});


ProductNotificationsSchema = mongoose.model('product_history', ProductNotificationsSchema);



var ProductSchema  = new Schema({
    sector: String,
    name: String,
    sector_info: {type: mongoose.Schema.Types.ObjectId, ref: ''},
    points: {type: Number, default:1},
    contract_state:  String,
    observations: {type: String, default: ''},
    payment_received_date: { type: Date, default: null }, //This is the date when payment was recieved for the product sold
    document_name_1: {type:String, default:'Hoja rescate de datos'},
    document_name_2: {type:String, default:''},
    document_name_3: {type:String, default:''},
    document_1: {type:Schema.Types.Mixed, default: undefined},
    document_2: {type:Schema.Types.Mixed, default: undefined},
    document_3: {type:Schema.Types.Mixed, default: undefined},
    personal_commission_payment:{type:Schema.Types.Mixed, default:false},
    personal_commission: {type:Number, default: 0.00},
    shared_commission: {type:Number, default: 0.00},
    pro_supervisor_commission: {type:Number, default: 0.00},
    product_notifications:[ProductNotificationsSchema.schema],
    pro_supervisor_info: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, //This references the User collection o model
    client_info:  {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, //This references the User collection o model
    supervisor_info: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, //This references the User collection o model
    created: { type: Date, default: Date.now }
});

//ProductSchema.set('autoIndex', false);
//ProductSchema.index({ created: 1});
ProductSchema = mongoose.model('Products', ProductSchema);



module.exports = ProductSchema ;
