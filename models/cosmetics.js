var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var CosmeticsSchema  = new Schema({
    provider: String,
    provider_email: String,
    product: {type:String,index: true},
    points:  String,
    created: { type: Date, default: Date.now }
});

CosmeticsSchema.index({product: 'text'}); //Indexing for fast db queries and text search
Cosmetics = mongoose.model('Cosmetics', CosmeticsSchema);

module.exports = Cosmetics ;
