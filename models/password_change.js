var mongoose            = require('mongoose');



var Schema              = mongoose.Schema;

var PasswordChangeSchema  = new Schema({
    hash: String,
    user_id: {type:String,default:false},
    email: {type:String,default:false},
    created: { type: Date, default: Date.now }
});


PasswordChange = mongoose.model('password_change', PasswordChangeSchema);

module.exports = PasswordChange ;
