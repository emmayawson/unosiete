var trim                    = require('trim');
var bcrypt 			        = require('bcrypt-nodejs');
var express                 = require('express');
var app                     = express();

var User                    = require('../../models/user.js');

module.exports = {

    index: function(req, res){
        res.render('./supervisor/login');
    },

    post: function(req, res){
        var user = req.body;
        //console.log(user);

        if (trim(user.email) == '' || trim(user.password) == '') {
            var msg = req.flash('info', 'Email/contraseña requerido para inicar sesion');
            //console.log(msg);
            res.render('./supervisor/login', {message: msg});
            res.end();
            return;
        }


        User.findOne({email: user.email}, function (err, userDoc) {
            if (err) {
                return next(err);
            }

            //console.log(userDoc);
            if (userDoc) {
                bcrypt.compare(user.password, userDoc.password, function (err, results) {

                    if (err) {
                        return next(err);
                    }

                    //console.log(results);
                    //console.log(userDoc);
                    if (results) {

                        if(userDoc.authorized !== false){
                            req.user = userDoc;
                            delete req.user.password ;
                            req.session.user = req.user;
                            //app.locals.user = userDoc;
                            //console.log(req.session);

                            res.redirect(301, '/supervisor/dashboard');

                        } else {
                            var msg = req.flash('error', 'Su cuenta aún no ha sido authorizado para poder utilizarse, ' +
                                'por favor pongase en contacto con su administrador para que te activen la cuenta!');
                            res.render('./supervisor/login');
                            res.end();
                        }




                    } else {
                        var msg = req.flash('error', 'Contraseña no es correcto');
                        res.render('./supervisor/login');
                        res.end();
                    }

                });

            } else {
                var msg = req.flash('error', 'Los datos introducidos son erroneos');
                res.render('./supervisor/login');
                res.end();
            }


        });;
    }

}