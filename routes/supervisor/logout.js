var express                 = require('express');
var app                     = express();

module.exports = {

    index: function (req, res) {

        app.locals.user = null;
        res.locals.user = null;
        req.session.destroy();
        res.redirect(301, '/supervisor/login');
    }
}