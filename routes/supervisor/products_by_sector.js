var Telecom                      = require('../../models/telecommunication.js');
var Investment                   = require('../../models/investment.js');
var Insurance                    = require('../../models/insurance.js');
var Maintenance                  = require('../../models/maintenance.js');
var SmallBusiness                = require('../../models/small_business.js');
var Ecommerce                    = require('../../models/ecommerce.js');
var Cosmetics                    = require('../../models/cosmetics.js');


module.exports = {

    productsBySector: function(req, res){
       // console.log(req.body);

        switch (req.body.sector) {
            case 'Telecomunicación & Energías':
                Telecom.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;
            case  'Ahorro & Inversión':
                Investment.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;
            case 'Seguros  Generales':
                Insurance.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;
            case 'Mantenimiento & Reparaciones':
                Maintenance.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;
            case 'Autónomos & Empresa':
                SmallBusiness.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;
            case 'Alimantación & E-commerce':
                Ecommerce.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;
            case 'Hogar & Bienestar':
                Cosmetics.find({},'product', function(err, products){
                    //console.log(clients);
                    if(!err){
                        res.json({products: products});
                        res.end();

                    }else{
                        res.json({error: 'No se pudó obtener el listado de productos'});
                        res.end();
                        //console.log(err);
                    }
                });
                break;


        }


    }
};
