//var express                 = require('express');
//var router                  = express.Router();
//var app                     = express();

var User                      = require('../../../../models/user.js');

module.exports = {

    index: function(req, res){

        User.find({created_by: req.session.user._id, $where:"this.client == true"}, function(err, clients){
            //console.log(clients);
            if(!err){

                res.render('./supervisor/dashboard/clients/index', {
                    heading:'Listado de clientes',
                    clients: clients

                });

            }else{
                return next(err);
            }
        });

    }
};
