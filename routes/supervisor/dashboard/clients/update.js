var moment                              = require('moment');
var trim                                = require('trim');
var UserModel                           = require('../../../../models/user.js');
//console.log(require('./models/user.js'));


module.exports = {

    update: function(req, res){
        var item = req.body;
        //console.log(req.params.id);
        //console.log(item);
        //Check if the DNI/NIE  exists

        UserModel.update({_id:req.params.id}, {$set: {
         first_name: item.first_name,
         last_name: item.last_name,
         identification_number: item.identification_number,
         email: item.email,
         telephone: item.telephone,
         mobile: item.mobile,
         address: item.address,
         city: item.city,
         post_code: item.post_code,
         date_of_birth: moment(item.date_of_birth,'DD/MM/YYYY').utc().format(),
         client: true,
         sex: item.sex

        }},function(err, info){


                if(!err){
                    res.json({message:'OK'});
                } else {
                    //console.log(err);
                    res.json({error: 'Error, vuelva a intentarlo más tarde'});
                }



        });



    }
};

