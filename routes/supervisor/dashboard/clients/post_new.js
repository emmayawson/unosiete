var moment                              = require('moment');
var trim                                = require('trim');
var UserModel                           = require('../../../../models/user.js');
//console.log(require('./models/user.js'));


module.exports = {

    post: function(req, res){
        var item = req.body;
        //console.log( item);
        //Check if the DNI/NIE  exists
        UserModel.findOne({identification_number:item.identification_number}, function(err, user){
            // console.log( req.session.user._id);

            if(user && user.email ===  trim(item.email)){ //Check if the email  exists
                res.json({error: 'Este email ya esta registrado a otro usuario'});
                res.end();

            }else if(user && user.identification_number === trim(item.identification_number) ){
                res.json({error: 'Este DNI/DIE ya esta registrado a otro usuario'});
                res.end();

            } else {
              //  console.log( moment(item.date_of_birth,'DD/MM/YYYY').utc().format());
                if(req.session && req.session.user){
                    var client = new UserModel({
                        first_name: item.first_name,
                        last_name: item.last_name,
                        identification_number: item.identification_number,
                        email: item.email,
                        telephone: item.telephone,
                        mobile: item.mobile,
                        address: item.address,
                        city: item.city,
                        post_code: item.post_code,
                        date_of_birth: moment(item.date_of_birth,'DD/MM/YYYY').utc().format(),
                        client: true,
                        sex: item.sex,
                        created_by: req.session.user._id

                    });

                } else {
                    res.json({info: 'login'});
                }

                client.save(function (err) {
                    if (!err) {
                        res.json({message: 'OK'});
                        res.end();
                    } else {
                        res.json({error: 'Error, no se creó el nuevo cliente. Intentelo de nuevo más tarde'});
                        res.end();
                    }
                });
            }

        });

    }
}
