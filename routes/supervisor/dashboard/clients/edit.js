var moment                              = require('moment');
var trim                                = require('trim');
var UserModel                           = require('../../../../models/user.js');


module.exports = {

    edit: function(req, res){
        User.findOne({_id:req.params.id},function(err, client){
            if(!err){
                res.render('./supervisor/dashboard/clients/edit',{
                    heading:'Editar cliente',
                    client:client
                });
            } else{
                return next(err);
            }
        });

    }
}
