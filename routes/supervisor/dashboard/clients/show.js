var User                      = require('../../../../models/user.js');
var Product                   = require('../../../../models/product.js');

module.exports = {

    show: function(req, res){

        User.findOne({_id: req.params.id}, function(err, client){
            //console.log(clients);
            if(!err){

                User.find({$where:"this.supervisor == true || this.admin == true"},function(err, supervisors){
                    //console.log(clients);
                    if(!err){


                        //get client products
                        Product.find({client_info: client._id}).sort({created: -1}).exec(function(err, clientProducts){
                            //console.log(clientProducts );

                            res.render('./supervisor/dashboard/clients/show', {
                                heading:'Información cliente',
                                client: client,
                                supervisors: supervisors,
                                clientProducts:clientProducts
                            });

                        });



                        //res.json({supervisors: supervisors});
                        //res.end();

                    }else{
                        res.json({error: {}});
                        res.end();
                        return next(err);
                    }
                });

             /*
                res.render('./supervisor/dashboard/clients/show', {
                    heading:'Información cliente',
                    client: client
                });
                */

            }else{
                return next(err);
            }
        });

    }
};
