var express                 = require('express');
var router                  = express.Router();
var app                     = express();


var moment                              = require('moment');
var flatten                             = require('array-flatten');
var async                               = require('async');
var trim                                = require('trim');
var Product                             = require('../../../models/product.js');


module.exports = {

    index: function(req, res){
        //Get current month
        //var currentMonth = new Date().getMonth() + 1;
        //console.log(currentMonth);

        //Months to be used in the view to display the mont as string
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];

        //Get Telecom shared
        async.parallel({
            telecom:function(callback){
              Product.aggregate([
                {$match:{
                    sector: 'Telecomunicación & Energías'
                }},
                {$match:{'contract_state_history.updated': function() {
                    var currentMonth = new Date().getMonth() + 1;
                    return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                }}},
                {$match:{
                    'contract_state_history.state': 'Comisionable'
                }},
                {
                    $group: {
                        _id: null,
                        totalSharedCommisionTelecom: {$sum: '$shared_commission'}
                    }
                }

            ], function(err, telecomResults){
                if(!err){
                    callback(err, telecomResults);
                    return telecomResults;
                } else {
                    return next(err);
                }
            })

        } ,

             insurance: function(callback){
                 Product.aggregate([
                     {$match:{
                         sector: 'Seguros  Generales'
                     }},
                     {$match:{'contract_state_history.updated': function() {
                         var currentMonth = new Date().getMonth() + 1;
                         return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                     }}},
                     {$match:{
                         'contract_state_history.state': 'Comisionable'
                     }},
                     {
                         $group: {
                             _id: null,
                             totalSharedCommisionInsurance: {$sum: '$shared_commission'}
                         }
                     }

                 ], function(err, investmentResults){
                     if(!err){
                         callback(err, investmentResults);
                         return investmentResults;

                     } else {
                         return next(err);
                     }
                 })
             }, //End insurance

            investment: function(callback){
                Product.aggregate([
                    {$match:{
                        sector: 'Ahorro & Inversión'
                    }},
                    {$match:{'contract_state_history.updated': function() {
                        var currentMonth = new Date().getMonth() + 1;
                        return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                    }}},
                    {$match:{
                        'contract_state_history.state': 'Comisionable'
                    }},
                    {
                        $group: {
                            _id: null,
                            totalSharedCommisionInvestment: {$sum: '$shared_commission'}
                        }
                    }

                ], function(err, investmentResults){
                    if(!err){
                        callback(err, investmentResults);
                        return investmentResults;

                    } else {
                        return next(err);
                    }
                })
            }, //end investment

            maintenance: function(callback){
                Product.aggregate([
                    {$match:{
                        sector: 'Mantenimiento & Reparaciones'
                    }},
                    {$match:{'contract_state_history.updated': function() {
                        var currentMonth = new Date().getMonth() + 1;
                        return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                    }}},
                    {$match:{
                        'contract_state_history.state': 'Comisionable'
                    }},
                    {
                        $group: {
                            _id: null,
                            totalSharedCommisionMaintenance: {$sum: '$shared_commission'}
                        }
                    }

                ], function(err, maintenanceResults){
                    if(!err){
                        callback(err, maintenanceResults);
                        return maintenanceResults;

                    } else {
                        return next(err);
                    }
                })
            }, //maintenance

            small_business: function(callback){
                Product.aggregate([
                    {$match:{
                        sector: 'Autónomos & Empresa'
                    }},
                    {$match:{'contract_state_history.updated': function() {
                        var currentMonth = new Date().getMonth() + 1;
                        return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                    }}},
                    {$match:{
                        'contract_state_history.state': 'Comisionable'
                    }},
                    {
                        $group: {
                            _id: null,
                            totalSharedCommisionSmallBusiness: {$sum: '$shared_commission'}
                        }
                    }

                ], function(err, small_businessResults){
                    if(!err){
                        callback(err, small_businessResults);
                        return small_businessResults;

                    } else {
                        return next(err);
                    }
                })
            }, //end small_business

            ecommerce: function(callback){
                Product.aggregate([
                    {$match:{
                        sector: 'Alimantación & E-commerce'
                    }},
                    {$match:{'contract_state_history.updated': function() {
                        var currentMonth = new Date().getMonth() + 1;
                        return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                    }}},
                    {$match:{
                        'contract_state_history.state': 'Comisionable'
                    }},
                    {
                        $group: {
                            _id: null,
                            totalSharedCommisionEcommerce: {$sum: '$shared_commission'}
                        }
                    }

                ], function(err, ecommerceResults){
                    if(!err){
                        callback(err, ecommerceResults);
                        return ecommerceResults;

                    } else {
                        return next(err);
                    }
                })
            }, //end ecommerce
            cosmetics: function(callback){
                Product.aggregate([
                    {$match:{
                        sector: 'Hogar & Bienestar'
                    }},
                    {$match:{'contract_state_history.updated': function() {
                        var currentMonth = new Date().getMonth() + 1;
                        return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                    }}},
                    {$match:{
                        'contract_state_history.state': 'Comisionable'
                    }},
                    {
                        $group: {
                            _id: null,
                            totalSharedCommisionCosmetics: {$sum: '$shared_commission'}
                        }
                    }

                ], function(err, cosmeticsResults){
                    if(!err){
                        callback(err, cosmeticsResults);
                        return cosmeticsResults;

                    } else {
                        return next(err);
                    }
                })
            },

            telecomQualified: function(callback){
                Product.aggregate([
                    {$match:{
                        sector: 'Telecomunicación & Energías'
                    }},
                    {$match:{'contract_state_history.updated': function() {
                        var currentMonth = new Date().getMonth() + 1;
                        /* &&   17  <=  (new Date(this.contract_state_history.updated).getDate()) */
                        return (new Date(this.contract_state_history.updated).getMonth() + 1)  == currentMonth
                    }}},
                    {$match:{
                        'contract_state_history.state': 'Comisionable'
                    }},
                    {$match:{
                        supervisor_info: function(){
                            return this._id == userid
                        }
                    }},
                    {$group: {
                        _id: '$sector',
                        productsSoldCount: {$sum: 1}
                    }}
                ],function(err, telecomResults){
                    if(!err){
                        callback(err, telecomResults);
                        //console.log(telecomResults);
                        return telecomResults;
                    } else {
                        return next(err);
                    }
                })
            }




        }, function(err, results){
            //console.log(results.telecom[0].totalSharedCommisionTelecom);
            //console.log(results);
            //console.log(results.telecomQualified[0].productsSoldCount);
            res.render('./supervisor/dashboard/index',{
                monthNames:monthNames,
                telecomCommisions: (results.telecom[0] != undefined) ? results.telecom[0].totalSharedCommisionTelecom  : 0.00,
                insuranceCommisions: (results.insurance[0] != undefined) ? results.insurance[0].totalSharedCommisionInsurance : 0.00,
                investmentCommisions: (results.investment[0] != undefined) ? results.investment[0].totalSharedCommisionInvestment : 0.00,
                maintenanceCommisions: (results.maintenance[0] != undefined) ? results.maintenance[0].totalSharedCommisionMaintenance : 0.00,
                smallBusinessCommisions: (results.small_business[0] != undefined) ? results.small_business[0].totalSharedCommisionSmallBusiness : 0.00,
                ecommerceCommisions: (results.ecommerce[0] != undefined) ? results.ecommerce[0].totalSharedCommisionEcommerce : 0.00,
                cosmeticsCommisions: (results.cosmetics[0] != undefined) ? results.cosmetics[0].totalSharedCommisionCosmetics : 0.00,
                telecomQualified: (results.telecomQualified[0] != undefined) ? results.telecomQualified[0].productsSoldCount : 0,

            });


        });


    }
}
