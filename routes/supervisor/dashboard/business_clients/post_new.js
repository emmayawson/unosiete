var moment                              = require('moment');
var trim                                = require('trim');
var UserModel                           = require('../../../../models/user.js');
//console.log(require('./models/user.js'));


module.exports = {

    post: function(req, res){
        var item = req.body;

        //Check if the DNI/NIE  exists
        UserModel.findOne({identification_number:item.identification_number}, function(err, user){
            // console.log( req.session.user._id);

            if(user && user.email ===  trim(item.email)){ //Check if the email  exists
                res.json({error: 'Este email ya esta registrado a otro cliente'});
                res.end();

            }else if(user && user.identification_number === trim(item.identification_number) ){
                res.json({error: 'Este CIF/NIF ya esta registrado a otro cliente'});
                res.end();

            } else {
                if(req.session && req.session.user){
                    var client = new UserModel({
                        first_name: item.first_name,
                        contact_person: item.contact_person,
                        identification_number: item.identification_number,
                        email: item.email,
                        telephone: item.telephone,
                        mobile: item.mobile,
                        fax: item.fax,
                        address: item.address,
                        city: item.city,
                        post_code: item.post_code,
                        province: item.province,
                        /*client: true,*/
                        business_client: true,
                        created_by: req.session.user._id

                    });

                } else {
                    res.json({message: 'login'});
                }

                client.save(function (err) {
                    if (!err) {
                        res.json({message: 'OK'});
                        res.end();
                    } else {
                        res.json({error: 'Error, no se creó el nuevo cliente. Intentelo de nuevo más tarde'});
                        res.end();
                    }
                });
            }

        });

    }
}
