var User                      = require('../../../../models/user.js');
var Product                   = require('../../../../models/product.js');

module.exports = {

    show: function(req, res){

            if(req.session && req.session.user){

                User.findOne({_id: req.params.id}, function(err, client){
                    //console.log(client);
                    if(!err){

                        //User.find({$where:"this.supervisor == true || this.admin == true"},function(err, supervisors){
                        User.find({},function(err, supervisors){
                            //console.log(clients);
                            if(!err){


                                //get client products
                                Product.find({client_info: client._id}).sort({created: -1}).exec(function(err, clientProducts){
                                    //console.log(clientProducts );

                                    res.render('./supervisor/dashboard/business_clients/show', {
                                        heading:'Información empresa cliente',
                                        client: client,
                                        supervisors: supervisors,
                                        clientProducts:clientProducts
                                    });

                                });



                                //res.json({supervisors: supervisors});
                                //res.end();

                            }else{
                                //res.json({error: {}});
                                //res.end();
                                return next(err);
                            }
                        });



                    }else{
                        return next(err);
                    }
                });

            } else {
                res.redirect(301, '/login');
            }



    }
};
