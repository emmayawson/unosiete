var moment                              = require('moment');
var trim                                = require('trim');
var UserModel                           = require('../../../../models/user.js');
//console.log(require('./models/user.js'));


module.exports = {

    update: function(req, res){
        var item = req.body;
        //console.log(req.params.id);
        //console.log(item);
        //Check if the CIF/NIF  exists

        UserModel.update({_id:req.params.id}, {$set: {
         first_name: item.first_name,
         contact_person: item.contact_person,
         identification_number: item.identification_number,
         email: item.email,
         telephone: item.telephone,
         mobile: item.mobile,
         fax: item.fax,
         address: item.address,
         city: item.city,
         post_code: item.post_code,
         province: item.province

        }},function(err, info){

                if(!err){
                    res.json({message:'OK'});
                } else {
                   // console.log(err);
                    res.json({error: 'Error, vuelva a intentarlo más tarde o consulta el tecnico responsable'});
                }

        });

    }
};

