var User                      = require('../../../../models/user.js');
var Product                   = require('../../../../models/product.js');


module.exports = {

    edit: function(req, res) {
        //console.log(req.query);

            Product.find({_id: req.params.id}, function (err, editProduct) {
                if(!err){
                    User.find({$where: "this.supervisor == true || this.admin == true"}, function (err, supervisors) {

                        if (!req.query) {
                            res.render('./supervisor/dashboard/products/edit',
                                {
                                    heading: 'Editar producto',
                                    editProduct: editProduct,
                                    supervisors: supervisors
                                });
                        } else {
                            res.render('./supervisor/dashboard/products/edit',
                                {
                                    heading: 'Editar producto',
                                    editProduct: editProduct,
                                    supervisors: supervisors,
                                    redirectMessage: 'Suba los documentos necesarios'
                                });
                        }

                    });

                }else{
                    return next(err);
                }
            })


    }
};
