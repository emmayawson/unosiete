var moment                              = require('moment');
//var Product                             = require('../../../../models/product.js');
//console.log(require('./models/user.js'));




exports.updateProductState = function (Product){

    return function(req, res){
        var item = req.body;
            //console.log(new Date().toISOString());
            //console.log(item);

        if(item.contractState == 'Comisionable') {
        //TODO Store each date the product was modified  in contract_state_history or some other collection
            Product.update({_id: item.product},
                    {$set: {
                        contract_state: item.contractState,
                        personal_commission : item.personalCommision,
                        shared_commission  : item.sharedCommision,
                        pro_supervisor_commission :item.proCommision,
                        contract_state_history: {state: item.contractState, updated: new Date().toISOString()}
                    }
                },
                function(err, doc){

                if(!err){

                    res.json({message:'OK'});
                    res.end();
                    //});

                } else {

                    res.json({message:'error'});
                    //return next(err);

                    //res.end();

                }

            });


        } else {

            Product.update({_id: item.productId},
                {$set:{contract_state: item.contractState, contract_state_history: {state: item.contractState, updated: new Date().toISOString()}}},
                function(err, doc){
                //TODO Store each date the product was modified in contract_state_history or some other collection
                if(!err){
                    //var newSate = new
                    //Product.find({_id: item.productId}, function(err, product){

                    //product.update.contract_state_history.push({state: item.contractState, updated: new Date().toISOString()});
                    res.json({message:'OK'});
                    res.end();
                    //});

                } else {
                    //console.log(err);
                    res.json({message:'error'});
                    //return next(err);
                    //res.end();

                }

            });

            }


    }

};
