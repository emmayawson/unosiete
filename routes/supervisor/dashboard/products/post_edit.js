var User                      = require('../../../../models/user.js');
var Product                   = require('../../../../models/product.js');
var moment                    = require('moment');


module.exports = {

    put: function(req, res){
       // var item = req.body;

        //console.log( req.session.user._id);
        //console.log( item);

        if(req.session && req.session.user){
            var item = req.body;
            Product.update({_id:req.params.id},{$set:{
                sector: item.product_sector,
                name: item.product_name,
                start_date:  moment(item.start_date,'DD/MM/YYYY').utc().format(),
                finish_date: moment(item.finish_date,'DD/MM/YYYY').utc().format(),
                contract_state: item.contract_state,
                pro_supervisor_id: item.pro_support
            }},  function(err, product){
                //console.log(product);
                if (!err) {
                    res.json({message: 'OK'});
                    res.end();
                } else {
                    res.json({error: 'Error, no se modificó  el producto. Intentelo de nuevo más tarde'});
                    res.end();
                }
            });


        } else {
            res.json({info: 'login'});
        }


    }
}