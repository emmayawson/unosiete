
$(document).ready(function(){

    //Add datatable to recieved pro  commission table
    $('#supervisor-recieved-pro-commission-list').DataTable();

    //Add datatable to the paid personale commission table
    $('#supervisor-recieved-personal-commission-list').DataTable({
        // "order": [[ 6, "desc" ]] /* Sort the datetime column */
    });

    //Get sub superviors sales info and display it on click from the supervisors dashboard
    $('.sub-supervisor-id').on('click', function(){
        spinnerStart();
        var name = $(this).text() ;

        $.ajax({
            method: "POST",
            url: "/supervisor/punctuation/sub-supervisor-sales-info",
            dataType:'json',
            data:{subSupervisorId:$(this).attr('data-id')}
        }).done(function(results) {

            if(results.message === 'OK'){

                spinnerStop();
               // $('#row-to-delete').remove();
                var display  = '<tr id="row-to-delete">';
                    display += '<td><b>  '+  name +'</b></td>';
                    display += '<td>' + results.telecom + '</td>';
                    display += '<td>' + results.insurance + '</td>';
                    display += '<td>' + results.investment + '</td>';
                    display += '<td>' + results.small_business + '</td>';
                    display += '<td>' + results.maintenance + '</td>';
                    display += '<td>' + results.ecommerce + '</td>';
                    display += '<td>' + results.cosmetics + '</td>';
                    display += '</tr>';

                $(display).insertAfter('#sub-supervisor-data');

            }else if(results.error){
                 spinnerStop();

                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });


            }

        });


    });


    //Recover supervisor password, new password creation
    $('#change-password-form input[name=password_confirm]').on('keyup', function(evt){
      var pass            = $('#change-password-form input[name=password]').val();
      var passConfirm     = $('#change-password-form input[name=password_confirm]').val();
      
      if(passConfirm !== pass){
          $('#recover-password-create').prop( "disabled", true );
          $('#error-message').empty();
          $('#error-message').append('Las contraseñas no coinciden');
      }else{
            $('#error-message').empty();
            $('#recover-password-create').prop( "disabled", false );
      }

    });

    //Recover forgotten password
    $('#recover-password').on('click submit', function(evt){
        evt.preventDefault();

        var email = $('#email').val();

        if(email){
            $.ajax({
                method: "POST",
                url: "/supervisor/recover-password",
                dataType:'json',
                data:{email:email}
            }).done(function(results) {

                if(results.message === 'OK'){
                    //spinnerStop();
                    $.jGrowl("Recibirás un email con un enlace para crear tu nueva contraseña!");

                    //location.reload();
                    //window.location.href = '/supervisors/products/edit/' + results.productId + '?redirect_msg=documents'

                }else if(results.error){
                   // spinnerStop();

                    $.jGrowl(results.error);


                }

            });

        }else{
            $.jGrowl("Introduzca su email!");
        }
    });


    //Edit recommended  supervisor form
    $('form#recomended-supervisor-edit-form').validate({
        rules:{
            first_name: {required:true},
            last_name: {required:true},
            identification_number: {required:true},
            email: {required:true},
            mobile: {required:true},
            address: {required:true},
            city: {required:true},
            post_code: {required:true},
            date_of_birth: {required:true},
            account_creation_state: {required:true}
        },

        messages:{
            first_name: {required: 'Introduzca un nombre por favor!'},
            last_name: {required: 'Introduzca un apellido por favor!'},
            identification_number: {required: 'Introduzca un DNI/NIE por favor!'},
            email: {required: 'Introduzca un email por favor!'},
            mobile: {required: 'Introduzca un número por favor!'},
            address: {required: 'Introduzca una dirección por favor!'},
            city: {required: 'Introduzca una localidad por favor!'},
            post_code: {required: 'Introduzca una código postal por favor!'},


        },

        submitHandler: function() {
            var data = $('form').serialize();
            var userId = $('input#user_id').val();
            //console.log(userId);
            spinnerStart();
            $.ajax({
                method: "PUT",
                url: "/supervisor/recommend-supervisor/edit/" + userId,
                dataType:'json',
                data:data
            }).done(function(results) {
                if(results.message){
                    spinnerStop();
                    $.pnotify({
                        title: 'Exito',
                        text: results.message,
                        type: 'success'
                    });
                }else {
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                }



                //alert(results.message);
            });


        }
    });

    $('form#recommend-supervisor-form').validate({
        rules:{
            first_name: {required:true},
            last_name: {required:true},
            identification_number: {required:true},
            email: {required:true},
            mobile: {required:true},
            address: {required:true},
            city: {required:true},
            post_code: {required:true},
            date_of_birth: {required:true},
            account_creation_state: {required:true}
        },

        messages:{
            first_name: {required: 'Introduzca un nombre por favor!'},
            last_name: {required: 'Introduzca un apellido por favor!'},
            identification_number: {required: 'Introduzca un DNI/NIE por favor!'},
            email: {required: 'Introduzca un email por favor!'},
            mobile: {required: 'Introduzca un número por favor!'},
            address: {required: 'Introduzca una dirección por favor!'},
            city: {required: 'Introduzca una localidad por favor!'},
            post_code: {required: 'Introduzca una código postal por favor!'},
            date_of_birth: {required: 'Introduzca una fecha de nacimiento por favor!'},
            account_creation_state: {required: 'Indica el estado de la solicitud por favor!'}
        },
        submitHandler: function() {

           // evt.preventDefault();
            var fd = new FormData(document.querySelector("#recommend-supervisor-form"));

            spinnerStart();
            $.ajax({
                method: "POST",
                url: "/supervisor/recommend-supervisor/new",
                processData: false,
                contentType: false,
                data:fd
            }).done(function(results) {
                if(results.message){
                    spinnerStop();
                    $.pnotify({
                        title: 'Exito',
                        text: results.message,
                        type: 'success'
                    });
                }else {
                    //console.log(results);
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                }



                //alert(results.message);
            });


        }

    });

    $('#product-notification-save-btn').on('click', function(evt){
        evt.preventDefault();
        //var data = $('#new-client-product-observation-form').serialize();
        var fd = new FormData(document.querySelector("#new-client-product-observation-form"));

        spinnerStart();


        $.ajax({
            method: "POST",
            url: "/supervisor/product-notification/new/",
            processData: false,
            contentType: false,
            data:fd
        }).done(function(results) {
            //console.log( results);
            if(results.message === 'OK'){
                spinnerStop();
                $.pnotify({
                    title: 'Exito',
                    text: 'Observaciones guardados correctamente',
                    type: 'success'
                });

               //location.reload();
               // window.location.href = '/supervisors/products/edit/' + results.productId + '?redirect_msg=documents'

            }else if(results.error){
                spinnerStop();
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            } else if(results.info === 'login'){
                window.location.href = '/supervisor/login'
            }

        });



       // alert(data);

    });

    //SUPERVISOR: Crete new product for a customer
    $("#new-client-product-form").submit(function(evt) {
        evt.preventDefault();
        $("#new-product-btn").attr('disabled', true);
        spinnerStart();
        var fd = new FormData(document.querySelector("#new-client-product-form"));
        console.log(fd);

        $.ajax({
            method: "POST",
            url: "/supervisors/products/new",
            processData: false,
            contentType: false,
            data:fd
        }).done(function(results) {
            $("#new-product-btn").attr('disabled', false);
            if(results.message === 'OK'){
                spinnerStop();
                $("#new-client-product-form").trigger("reset");
                $.pnotify({
                    title: 'Exito',
                    text: 'Producto creado correctamente',
                    type: 'success'
                });


                //location.reload();
                //window.location.href = '/supervisors/products/edit/' + results.productId + '?redirect_msg=documents'

            }else if(results.error){
                spinnerStop();
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });



            } else if(results.info === 'login'){ //redirect to login page
                spinnerStop();

                window.location.href = '/supervisor/login'
            }

        });


    });

    //SUPERVISOR: Edit new product for a customer
    $('#edit-client-product-form').validate({

        submitHandler: function() {
            spinnerStart();
            var data = $('#edit-client-product-form').serialize();
            var product_id = $('#product_id').val();
            //console.log(data);

            $.ajax({
                method: "PUT",
                url: "/supervisors/products/edit/" + product_id,
                dataType:'json',
                data:data
            }).done(function(results) {
                if(results.message === 'OK'){
                    spinnerStop();
                    $.pnotify({
                        title: 'Exito',
                        text: 'Producto modificado correctamente',
                        type: 'success'
                    });

                    //location.reload();

                }else if(results.error){
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                } else if(results.info === 'login'){
                    spinnerStop();
                    window.location.href = '/supervisor/login'
                }

            });

        }
    });

    // Load products based on the sector selected
    $('#product_sector').on('change', function(){

        var sector = $('#product_sector').val();
        //console.log(sector);

        $.ajax({
            method: "POST",
            url: "/supervisor/dashboard/products-by-sector",
            dataType:'json',
            data:{sector:sector}
        }).done(function(results) {
            if(results.products){
                $('#product_name').empty();
                $.each(results.products, function(index, item){
                    //console.log(item);
                    $('#product_name').append('<option value=" ' + item.product +' ">'+ item.product + '</option>');
                });


            }else if(results.error){
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }


        });



    });

    //SUPERVISOR: Create new client
    $('#supervisor-create-client-form').validate({
        submitHandler: function() {
            var data = $('#supervisor-create-client-form').serialize();
            //console.log(data);
            spinnerStart();

            $.ajax({
                method: "POST",
                url: "/supervisor/clients/new",
                dataType:'json',
                data:data
            }).done(function(results) {
                spinnerStop();
                if(results.message === 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Cliente creado correctamente',
                        type: 'success'
                    });

                    setTimeout(function(){
                        location.href = "/supervisor/clients/index";
                    }, 2000);

                }else if(results.error){
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                } else if(results.info === 'login'){
                    spinnerStop();
                    window.location.href = '/supervisor/login'
                }

            });

        }
    });

    //SUPERVISOR: editar o modificar cliente
    $('#supervisor-edit-client-form').validate({
        submitHandler: function() {
            var client_id = $('#client_id').val();
            var data = $('#supervisor-edit-client-form').serialize();
            spinnerStart();


            $.ajax({
                method: "PUT",
                url: "/supervisors/clients/edit/" + client_id,
                dataType:'json',
                data:data
            }).done(function(results) {
                spinnerStop();
                if(results.message === 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Cliente modificado correctamente',
                        type: 'success'
                    });

                    setTimeout(function(){
                        location.href = "/supervisor/clients/edit/" + client_id;
                    }, 2000);

                }else if(results.error){
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                } else if(results.info === 'login'){
                    spinnerStop();
                    window.location.href = '/supervisor/login'
                }

            });

        }
    });

    //SUPERVISOR: Create new business client
    $('#supervisor-create-business-client-form').validate({
        submitHandler: function() {
            //Disable button on click
            $("#new-product-btn").attr('disabled', true);
            var data = $('#supervisor-create-business-client-form').serialize();
            spinnerStart();


            $.ajax({
                method: "POST",
                url: "/supervisor/business-clients/new",
                dataType:'json',
                data:data
            }).done(function(results) {
                spinnerStop();
                $("#new-product-btn").attr('disabled',false);
                if(results.message === 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Cliente creado correctamente',
                        type: 'success'
                    });

                    setTimeout(function(){
                        location.href = "/supervisor/business-clients/index";
                    }, 2000);

                }else if(results.error){
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                } else if(results.info === 'login'){
                    spinnerStop();
                    window.location.href = '/supervisor/login'
                }

            });

        }
    });

    //SUPERVISOR: editar o modificar empresa cliente
    $('#supervisor-edit-business-client-form').validate({
        submitHandler: function() {
            var client_id = $('#client_id').val();
            var data = $('#supervisor-edit-business-client-form').serialize();
            spinnerStart();


            $.ajax({
                method: "PUT",
                url: "/supervisor/business-clients/edit/" + client_id,
                dataType:'json',
                data:data
            }).done(function(results) {
                spinnerStop();
                if(results.message === 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Empresa cliente modificado correctamente',
                        type: 'success'
                    });

                    setTimeout(function(){
                        location.href = "/supervisor/business-clients/show/" + client_id ;
                    }, 2000);


                }else if(results.error){
                    spinnerStop();
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                } else if(results.info === 'login'){
                    spinnerStop();
                    window.location.href = '/supervisor/login'
                }

            });

        }
    });

});
