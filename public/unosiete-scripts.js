
jQuery(document).ready(function($) {
    console.log( "Jquery ready!" );
    //Global validator setup
    jQuery.validator.setDefaults({
        debug: true
    });


    //Admin delete product
    $('#admin-delete-product').on('click', function(evt){
        evt.preventDefault();

        //alert($(this).attr('data-product-id'));


        if(confirm('Estas seguro de que quieres borrar este producto de forma permamente')){
            //alert('SI');

            var productId         =  $(this).attr('data-product-id');

            //console.log(adminUser);
            //console.log(supervisorUser);
            var data = {product_id: productId};

            $.ajax({
                method: "POST",
                url: "/admin/product/delete/",
                dataType:'json',
                data:data
            }).done(function(results) {
                spinnerStop();


                if(results.message === 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Producto borrado.',
                        type: 'success'
                    });
                }else if(results.error) {
                    //console.log(results);
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                }else if(results.info === 'login'){
                    location.href = '/login';
                }


            });




        }
        else{
            $.pnotify({
                title: 'Info',
                text: 'Producto no borrado.',
                type: 'info'
            });
        }


    });

    //Datatables global default settings
    $.extend( true, $.fn.dataTable.defaults, {
        "deferRender": true,
        "lengthChange": true,
        "pageLength": 50, /* itemes */
        "bSort": false, /* turn off sorting by default */
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.8/i18n/Spanish.json"
        }
    } );


    //Admin pay supervisor personal commission button
    $('#admin_pay_personal_commission_btn').on('click', function(){


        if(confirm('Estas seguro de que quieres confirmar el pago de la comisión')){
            //alert('SI');

           var adminUser            =  $(this).attr('data-admin-id');
           var supervisorUser       =  $(this).attr('data-supervisor-id');
            //console.log(adminUser);
            //console.log(supervisorUser);
            var data = {admin: adminUser, supervisor: supervisorUser};

            $.ajax({
                method: "POST",
                url: "/admin/supervisor-personal-commission/pay",
                dataType:'json',
                data:data
            }).done(function(results) {
                spinnerStop();


                if(results.message === 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Comisión  pagado.',
                        type: 'success'
                    });
                }else if(results.error) {
                    //console.log(results);
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                }else if(results.info === 'login'){
                    location.href = '/login';
                }


            });




        }
        else{
           // alert('NOOp');
        }

    });


    //Supervisor personal commission request
    $('#supervisor_personal_commission_request_btn').on('click', function(){
        //alert('Good');
        var supervisorId = $(this).attr('data-supervisor-id');
        //console.log(supervisorId);

        $.ajax({
            method: "POST",
            url: "/supervisor/commissions/request-payment",
            dataType:'json',
            data:{supervisorId:supervisorId}
        }).done(function(results) {
            spinnerStop();


            if(results.message === 'OK'){
                $.pnotify({
                    title: 'Exito',
                    text: 'Solicitud de pago enviado.',
                    type: 'success'
                });
            }else if(results.error) {
                //console.log(results);
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }else if(results.info === 'login'){

                location.href = '/login';
            }


        });


    });


    //Add Datatables to Admin new contracts list
    $('#admin-new-contracts-list').DataTable({
       // "order": [[ 6, "desc" ]] /* Sort the datetime column */
    });

    //Add datatable to recomended supervisors list
    $('#admin-recomended-supervisors-list').DataTable({});


    //Add Datatables to Admin supervisors list
    $('#admin-supervisors-list').DataTable();

    //Add Datatables to Admin telecommunication contracts list
    $('#admin-telecommunication-contracts-list').DataTable();

    //Add Datatables to Admin telecommunication products list
    $('#admin-telecommunication-products-list').DataTable();

    //Add Datatables to Admin insurance contracts list
    $('#admin-insurance-contracts-list').DataTable();

    //Add Datatables to Admin insurance products list
    $('#admin-insurance-products-list').DataTable();

    //Add Datatables to Admin investments contracts list
    $('#admin-investments-contracts-list').DataTable();

    //Add Datatables to Admin investments products list
    $('#admin-investments-products-list').DataTable();

    //Add Datatables to Admin small_business contracts list
    $('#admin-small-business-contracts-list').DataTable();

    //Add Datatables to Admin small_business products list
    $('#admin-small-business-products-list').DataTable();

    //Add Datatables to Admin maintenance contracts list
    $('#admin-maintenance-contracts-list').DataTable();

    //Add Datatables to Admin maintenance products list
    $('#admin-maintenance-products-list').DataTable();

    //Add Datatables to Admin ecommerce contracts list
    $('#admin-ecommerce-contracts-list').DataTable();

    //Add Datatables to Admin ecommerce products list
    $('#admin-ecommerce-products-list').DataTable();

    //Add Datatables to Admin cosmetics contracts list
    $('#admin-cosmetics-contracts-list').DataTable();

    //Add Datatables to Admin cosmetics products list
    $('#admin-cosmetics-products-list').DataTable();



    //Change password check if both passwords are equal
    $('#change_password_form_btn').prop( "disabled", true );
    $('#change_password_form :input#password_confirm').on('blur change keyup keydown', function(){
        var pass =  $('#change_password_form :input#password').val();
        var pass_conf =  $('#change_password_form :input#password_confirm').val();


        if(pass !== '' && pass_conf !== ''){

            if(pass !== pass_conf){

                /*$.pnotify({
                    title: 'Error',
                    text:  'Sus contraseñas no son iguales!',
                    type: 'error'
                });
                */
                $('#change_password_form_btn').attr( "value", 'Sus contraseñas no son iguales aún' );
                $('#change_password_form_btn').prop( "disabled", true );

            }else if(pass === pass_conf ){
                $('#change_password_form_btn').attr( "value", 'Enviar' );
                $('#change_password_form_btn').prop( "disabled", false );

            }
        }

    });

    //Save new password
    $('#change_password_form').submit(function(evt){
        evt.preventDefault();
        var data = $(this).serialize();
        //alert(data);

        $.ajax({
            method: "POST",
            url: "/change-password/",
            dataType:'json',
            data:data
        }).done(function(results) {
            spinnerStop();


            if(results.message === 'OK'){
                $.pnotify({
                    title: 'Exito',
                    text: 'Contraseña cambiada con exito.',
                    type: 'success'
                });
            }else if(results.error) {
                //console.log(results);
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }else if(results.info === 'login'){
                location.href = '/login';
            }


        });


    });

    //SUPERVISOR clients list
    $('#supervisor-clients-list').DataTable();

    //SUPERVISOR business clients list
    $('#supervisor-business-clients-list').DataTable();


    //Admin create product notifications
    $('#admin-product-notification-form').submit(function(evt){

        evt.preventDefault();
        spinnerStart();
        var fd = new FormData(document.querySelector("#admin-product-notification-form"));
        //console.log(fd);

        $.ajax({
            method: "POST",
            url: "/admin/product-notification",
            processData: false,
            contentType: false,
            data:fd
        }).done(function(results) {
            spinnerStop();


            if(results){
                $.pnotify({
                    title: 'Exito',
                    text: 'Notificación creado.',
                    type: 'success'
                });
            }else if(results.error) {
                //console.log(results);
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }else if(results.info === 'login'){

                location.href = '/login';
            }


        });

    });



    $('#filename1').on('change keyup keydown blur ', function(){
        if($('#filename1').val() !== ''){
            $('#doc1').text($('#filename1').val());
            $('#fileupload1').removeAttr('disabled');

        }

    });


    $("#edit-client-product-file1-form").submit(function(evt){
        evt.preventDefault();
        spinnerStart();
        var fd = new FormData(document.querySelector("#edit-client-product-file1-form"));
       // console.log(fd);


        $.ajax({
            method: "POST",
            url: "/supervisor/clients-doc-upload-1",
            processData: false,
            contentType: false,
            data:fd
        }).done(function(results) {
            spinnerStop();
            $('#doc1').attr('href', results.secure_url);

            if(results){
                $.pnotify({
                    title: 'Exito',
                    text: 'Documento gurdado.',
                    type: 'success'
                });
            }else {
                console.log(results);
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }


        });

    });



    //Supervisor clients contract document upload 2
    //Check if the filename is not empty and enable the document field
    $('#filename2').on('change keyup keydown blur', function(){
        if($('#filename2').val() !== ''){
            $('#doc2').text($('#filename2').val());
            $('#fileupload2').removeAttr('disabled');
        }

    });


    $("#edit-client-product-file2-form").submit(function(evt){
        evt.preventDefault();
        spinnerStart();
        var fd = new FormData(document.querySelector("#edit-client-product-file2-form"));
        //console.log(fd);


        $.ajax({
            method: "POST",
            url: "/supervisor/clients-doc-upload-2",
            processData: false,
            contentType: false,
            data:fd
        }).done(function(results) {
            spinnerStop();
            $('#doc2').attr('href', results.secure_url);

            if(results){
                $.pnotify({
                    title: 'Exito',
                    text: 'Documento gurdado.',
                    type: 'success'
                });
            }else {
                console.log(results);
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }


        });

    });



    //Supervisor clients contract document upload 3
    //Check if the filename is not empty and enable the document field
    $('#filename3').on('change keyup keydown blur', function(){
        if($(this).val() !== ''){
            $('#doc3').text($('#filename3').val());
            $('#fileupload3').removeAttr('disabled');
        }

    });


    $("#edit-client-product-file3-form").submit(function(evt){
        evt.preventDefault();
        spinnerStart();
        var fd = new FormData(document.querySelector("#edit-client-product-file3-form"));
        //console.log(fd);


        $.ajax({
            method: "POST",
            url: "/supervisor/clients-doc-upload-3",
            processData: false,
            contentType: false,
            data:fd
        }).done(function(results) {
            spinnerStop();
            $('#doc3').attr('href', results.secure_url);

            if(results){
                $.pnotify({
                    title: 'Exito',
                    text: 'Documento gurdado.',
                    type: 'success'
                });
            }else {
                console.log(results);
                $.pnotify({
                    title: 'Error',
                    text: results.error,
                    type: 'error'
                });
            }


        });

    });



    //Admin approve supervisor recommendation
    $('#validate-supervisor-request').on('change',function() {

        var status = $('#validate-supervisor-request').val();
        var userid = $('#validate-supervisor-request').attr('data-user-id');

        if (status !== 'none') {

        if (confirm('Seguro que quires aceptar este nuevo asesor?')) {

            $.ajax({
                method: "POST",
                url: "/admin/supervisor/approve-supervisor",
                dataType: 'json',
                data: {status: status, userid: userid}
            }).done(function (results) {
                //$('#provedor').val(' ');
                //$('#producto').val(' ');
                //$('#puntuacion').val(' ');
                if (results.message === 'OK') {
                    $.pnotify({
                        title: 'Exito',
                        text: 'el proceso de aceptación de asesor completado !',
                        type: 'success'
                    });


                } else {
                    alert('error');
                }

            });

        }
    } else {
            $.pnotify({
                title: 'Aviso',
                text: 'Selecione un nuevo estado de cuenta !',
                type: 'error'
            });
        }
    });


    //Admin authorize account
    $('#authorize-account').on('change',function() {

        var status = $('#authorize-account').val();
        var userid = $('#authorize-account').attr('data-user-id');

        if (status !== 'none') {

            if (confirm('Seguro que quires activar esa cuenta?')) {

                $.ajax({
                    method: "POST",
                    url: "/admin/supervisor/authorize-account",
                    dataType: 'json',
                    data: {status: status, userid: userid}
                }).done(function (results) {
                    //$('#provedor').val(' ');
                    //$('#producto').val(' ');
                    //$('#puntuacion').val(' ');
                    if (results.message === 'OK') {
                        $.pnotify({
                            title: 'Exito',
                            text: 'El estado de la cuanta ha sido modificado corectamente !',
                            type: 'success'
                        });


                    } else {
                        alert('error');
                    }

                });

            }
        } else {
            $.pnotify({
                title: 'Aviso',
                text: 'Selecione un nuevo estado de cuenta !',
                type: 'error'
            });
        }
    });


    //Send email form www.unosiete.us web
    $('#-submit').on('click', function(evt){
        evt.preventDefault();
        var data = $('#-contact_form').serialize();

        $.ajax({
            method: "POST",
            url: "/contact-us",
            dataType:'json',
            data:data
        }).done(function(results) {
            //$('#provedor').val(' ');
            //$('#producto').val(' ');
            //$('#puntuacion').val(' ');

            $.pnotify({
                title: 'Exito',
                text: results.message,
                type: 'success'
            });

            if(results.message){
                console.log(results.message);
                $('#email_sent').append(results.message);
                $('#email_sent').show();
            }

        });
    });



    //Change product contract state
    $('.product-contract-state').on('change', function(){
      var contractState  = $(this).val();
      var productId      = $(this).attr('data-product-id');

      //var contractState  = $( "#product-contract-state option:selected" ).text();
        if(contractState === 'Comisionable'){
            $('#comisionable-save-btn').attr('data-product', $(this).attr('data-product-id')); //Current product ID to "comisionanble modal save button " button
            $('#comisionable-save-btn').attr('data-contract-state', $(this).val()); //Current contract state to "comisionanble modal save button " button

            $('#comisionable-modal').modal('show'); //The modal window is found in the admin dashboard view


        } else {

            if(confirm("Estas seguro de que quieres cambiar el estado de este producto")){


                $.ajax({
                    method: "POST",
                    url: "/admin/products/update-product-state",
                    dataType:'json',
                    data:{contractState:contractState,productId:productId}
                }).done(function(results) {

                    if(results.message == 'OK'){

                        $.pnotify({
                            title: 'Exito',
                            text: 'Estado de productos modificado',
                            type: 'success'
                        });

                    }
                    if(results.message === 'OK' && results.redirectToNotificationPage === true){
                        //console.log(results.product);
                        window.location.href = "/admin/product-notification?product_id=" + results.product._id + "&product_state="+results.product.contract_state;
                    }
                    if(results.error){

                        $.pnotify({
                            title: 'Error',
                            text: results.error,
                            type: 'error'
                        });

                    }


                });


            } else{
                $.pnotify({
                    title: 'Info',
                    text: 'El estado del producto no será modificado',
                    type: 'info'
                });
            }

        }


    });


    //Save comisionable and its data to database
    $("#comisionable-save-btn").on("click", function() {
        var personalCommision = $('#cartera-personal').val();
        var sharedCommision   = $('#facturacion-compartida').val();
        var proSupervisorCommission      = $('#support-pro-commission').val();
        var proSupervisorId   = $('#support-pro-supervisor').val();

        var product              =    $(this).attr('data-product');
        var contractState         =    $(this).attr('data-contract-state');
        //console.log(contractState,personalCommision,sharedCommision,proSupervisorCommission,proSupervisorId, product);
        $('#comisionable-modal').modal('hide');


        //Confirm product state change
        if(confirm("Quires guardar estas comiciones")){

            $.ajax({
                method: "POST",
                url: "/admin/products/update-product-state",
                dataType:'json',
                data:{
                    contractState:contractState,
                    product:product,
                    personalCommision:personalCommision,
                    sharedCommision:sharedCommision,
                    proSupervisorCommission:proSupervisorCommission,
                    proSupervisorId:proSupervisorId
                }
            }).done(function(results) {

                if(results.message == 'OK'){

                    $.pnotify({
                        title: 'Exito',
                        text: 'Estado de productos modificado y las comiciones guardadas',
                        type: 'success'
                    });

                } else {

                    $.pnotify({
                        title: 'Error',
                        text: 'Error, estado no modificado, intentelo de nuevo más tarde',
                        type: 'error'
                    });

                }


            });

        } else {

            $.pnotify({
                title: 'Info',
                text: 'El estado del producto no será modificado',
                type: 'info'
            });
        }


    });
    

    //Telecommunication add new product validation
    $('#telecommunication-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            console.log(provider,product,points);



            $.ajax({
                method: "POST",
                url: "/admin/telecommunication/new-product",
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });


    //Edit telecommunication product
    $('form#telecommunication-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            //var provider = $('#provedor').val();
            //var email    = $('#email').val();
            //var product  = $('#producto').val();
            //var points   = $('#puntuacion').val();
            //console.log(provider,product,points);



            var provider    = $('#provedor').val();
            var email       = $('#email').val();
            var product     = $('#producto').val();
            var points      = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            //console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/telecommunication/edit/" + productId ,
                dataType:'json',
                data:{email: email,provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });

        }
    });



    //Insurance add new product and validation
    $('form#insurance-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            console.log(provider,product,points);



            $.ajax({
                method: "POST",
                url: "/admin/insurance/new-product",
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });



    //Insurance edit product and validation
    $('form#insurance-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            //console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/insurance/edit/" + productId,
                dataType:'json',
                data:{email: email,provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });
        }
    });


    //Investment add new product and validation
    $('form#investment-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            console.log(provider,product,points);



            $.ajax({
                method: "POST",
                url: "/admin/investment/new-product",
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });


    //investment edit product and validation
    $('form#investment-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider    = $('#provedor').val();
            var email       = $('#email').val();
            var product     = $('#producto').val();
            var points      = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/investment/edit/" + productId,
                dataType:'json',
                data:{email: email,provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });
        }
    });


    //Small business add new product and validation
    $('form#small-business-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
           // console.log(provider,product,points);



            $.ajax({
                method: "POST",
                url: "/admin/small-business/new-product",
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });

    //Small business edit product and validation
    $('form#small-business-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/small-business/edit/" + productId,
                dataType:'json',
                data:{email:email, provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });
        }
    });




    //Maintenance add new product and validation
    $('form#maintenance-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            //console.log(provider,product,points);


            $.ajax({
                method: "POST",
                url: "/admin/maintenance/new-product",
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });


    //Maintenance edit product and validation
    $('form#maintenance-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            //console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/maintenance/edit/" + productId,
                dataType:'json',
                data:{email:email,provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });
        }
    });


    //Ecommerce add new product and validation
    $('form#ecommerce-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            //console.log(provider,product,points);



            $.ajax({
                method: "POST",
                url: "/admin/ecommerce/new-product",
                dataType:'json',
                data:{email:email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });


    //Ecommerce edit product and validation
    $('form#ecommerce-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            //console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/ecommerce/edit/" + productId,
                dataType:'json',
                data:{email: email,provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });
        }
    });


    //Cosmetics add new product and validation
    $('form#cosmetics-category-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            console.log(email,provider,product,points);



            $.ajax({
                method: "POST",
                url: "/admin/cosmetics/new-product",
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points}
            }).done(function(results) {
                $('#provedor').val(' ');
                $('#email').val(' ');
                $('#producto').val(' ');
                $('#puntuacion').val(' ');
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });

            });
        }
    });


    //Cosmetics edit product and validation
    $('form#cosmetics-category-edit-form').validate({
        rules:{
            provedor: {
                required:true
            },
            producto:{
                required:true
            },
            puntuacion:{
                required:true
            }
        },
        messages:{
            provedor    :{required:"Introduzca un provedor"},
            producto    :{required:"Introduzca un producto"},
            puntuacion  :{required:"Introduzca un la puntuación para este producto"}
        },
        submitHandler: function() {
            var provider = $('#provedor').val();
            var email    = $('#email').val();
            var product  = $('#producto').val();
            var points   = $('#puntuacion').val();
            var productId   = $('input#product-id').val();

            console.log(provider,product,points,productId);

            $.ajax({
                method: "PUT",
                url: "/admin/cosmetics/edit/" + productId,
                dataType:'json',
                data:{email: email, provider:provider,product:product,points:points, productId:productId}
            }).done(function(results) {
                $.pnotify({
                    title: 'Exito',
                    text: results.message,
                    type: 'success'
                });
                //alert(results.message);
            });
        }
    });


    //Create new supervisor form
    $('form#supervisor-form').validate({
        rules:{
         first_name: {required:true},
         last_name: {required:true},
         identification_number: {required:true},
         email: {required:true},
         mobile: {required:true},
         address: {required:true},
         city: {required:true},
         post_code: {required:true},
         date_of_birth: {required:true},
         account_creation_state: {required:true}
        },

        messages:{
            first_name: {required: 'Introduzca un nombre por favor!'},
            last_name: {required: 'Introduzca un apellido por favor!'},
            identification_number: {required: 'Introduzca un DNI/NIE por favor!'},
            email: {required: 'Introduzca un email por favor!'},
            mobile: {required: 'Introduzca un número por favor!'},
            address: {required: 'Introduzca una dirección por favor!'},
            city: {required: 'Introduzca una localidad por favor!'},
            post_code: {required: 'Introduzca una código postal por favor!'},
            date_of_birth: {required: 'Introduzca una fecha de nacimiento por favor!'},
            account_creation_state: {required: 'Indica el estado de la solicitud por favor!'}
        },

        submitHandler: function() {
            var data = $('form').serialize();
            //console.log('Ajax ok');
            //console.log(data);

            $.ajax({
                method: "POST",
                url: "/admin/supervisors/new",
                dataType:'json',
                data:data
            }).done(function(results) {
                if(results.message){
                    $.pnotify({
                        title: 'Exito',
                        text: results.message,
                        type: 'success'
                    });
                }else {
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                }



                //alert(results.message);
            });


        }
    });


    //Edit supervisor form
    $('form#supervisor-edit-form').validate({
        rules:{
            first_name: {required:true},
            last_name: {required:true},
            identification_number: {required:true},
            email: {required:true},
            mobile: {required:true},
            address: {required:true},
            city: {required:true},
            post_code: {required:true},
            date_of_birth: {required:true},
            account_creation_state: {required:true}
        },

        messages:{
            first_name: {required: 'Introduzca un nombre por favor!'},
            last_name: {required: 'Introduzca un apellido por favor!'},
            identification_number: {required: 'Introduzca un DNI/NIE por favor!'},
            email: {required: 'Introduzca un email por favor!'},
            mobile: {required: 'Introduzca un número por favor!'},
            address: {required: 'Introduzca una dirección por favor!'},
            city: {required: 'Introduzca una localidad por favor!'},
            post_code: {required: 'Introduzca una código postal por favor!'},
            date_of_birth: {required: 'Introduzca una fecha de nacimiento por favor!'},
            account_creation_state: {required: 'Indica el estado de la solicitud por favor!'}
        },

        submitHandler: function() {
            var data = $('form').serialize();
            var userId = $('input#user_id').val();
            console.log(userId);
            $.ajax({
                method: "PUT",
                url: "/admin/supervisors/edit/" + userId,
                dataType:'json',
                data:data
            }).done(function(results) {
                if(results.message){
                    $.pnotify({
                        title: 'Exito',
                        text: results.message,
                        type: 'success'
                    });
                }else {
                    $.pnotify({
                        title: 'Error',
                        text: results.error,
                        type: 'error'
                    });
                }



                //alert(results.message);
            });


        }
    });





    //Delete supervisor account
    $('.delete-supervisor').on('click', function(evt){
        var userId = $(this).attr('id');
        console.log(userId);
        if(confirm('Estas seguro de que quires eliminar esta cuenta? Es un proceso irevesible')){
            $.ajax({
                method: "DELETE",
                url: "/admin/supervisors/edit/" + userId,
                dataType:'json'
            }).done(function(results) {
                if(results.message == 'OK'){
                    $.pnotify({
                        title: 'Exito',
                        text: 'Asesor eliminado',
                        type: 'success'
                    });

                    setTimeout(function(){
                        window.location.href = '/admin/supervisors/index';
                    }, 3000);


                }else {
                    $.pnotify({
                        title: 'Error',
                        text:  'Error, no se eliminó  la cuenta, intentelo más tarde!',
                        type: 'error'
                    });
                }



                //alert(results.message);
            });
        }


    });






});